﻿var t;
(function () {
    angular.module("hotelApp", ['rzModule']);
    angular
       .module("hotelApp")
       .controller("hotelController", hotelController)
       .filter('range', function () {
           return function (input, total) {
               total = parseInt(total);

               for (var i = 0; i < total; i++) {
                   input.push(i);
               }

               return input;
           };
       })
      .directive('googleMap', function () {
          var link = function (scope, element, attrs) {
              var infoWindow, map;
              var markers = [];

              var mapOptions = {
                  center: new google.maps.LatLng(scope.data.Hotel_Response.Hotel_Details.Hotel[0].$.LAT, scope.data.Hotel_Response.Hotel_Details.Hotel[0].$.LONG),
                  zoom: scope.zoom,
                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                  scrollwheel: false
              };

              function initMap() {
                  if (map === void 0) {
                      map = new google.maps.Map(element[0], mapOptions);
                  }
              }

              function setMarker(map, position, title, content) {
                  var marker;
                  var markerOptions = {
                      position: position,
                      map: map,
                      title: title,
                      icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
                  };

                  marker = new google.maps.Marker(markerOptions);
                  markers.push(marker);

                  google.maps.event.addListener(marker, 'click', function () {
                      if (infoWindow !== void 0) {
                          infoWindow.close();
                      }
                      var infoWindowOptions = {
                          content: content
                      };
                      infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                      infoWindow.open(map, marker);
                  });
              }

              initMap();
              //console.log(scope.data);
              if (!scope.onemarker)
                  angular.forEach(scope.data.Hotel_Response.Hotel_Details.Hotel, function (hotel) {
                      setMarker(map, new google.maps.LatLng(hotel.$.LAT, hotel.$.LONG), hotel.$.HN, '<b><a href=Detail.html?hot_id=' + hotel.$.HID + '>' + hotel.$.HN + '</a></b><br/>Address: ' + hotel.$.Add);
                  });

          };

          return {
              restrict: 'A',
              template: '<div id="gmaps"></div>',
              replace: true,
              scope: {
                  data: "=mapData",
                  zoom: "=zoom",
                  onemarker: "=onemarker"
              },
              link: link
          };
      })
      .filter('hotelFilter', function () {
          return function (items, prop, asc) {
              //console.log(prop, asc);
              if (items != null && typeof (items) != 'undefined')
                  switch (prop) {
                      case 'StoreNameEnglish':
                      case 'IsActive':
                          return items.sort(function (a, b) {
                              if (asc) {
                                  return (a.$[prop] > b.$[prop]) ? 1 : ((a.$[prop] < b.$[prop]) ? -1 : 0);
                              } else {
                                  return (b.$[prop] > a.$[prop]) ? 1 : ((b.$[prop] < a.$[prop]) ? -1 : 0);
                              }
                          });
                          break;
                      case 'MIP':
                          return items.sort(function (a, b) {
                              if (asc) {
                                  return (parseInt(a.$[prop]) > parseInt(b.$[prop])) ? 1 : ((parseInt(a.$[prop]) < parseInt(b.$[prop])) ? -1 : 0);
                              } else {
                                  return (parseInt(b.$[prop]) > parseInt(a.$[prop])) ? 1 : ((parseInt(b.$[prop]) < parseInt(a.$[prop])) ? -1 : 0);
                              }
                          });
                          break;
                      default:
                          return items.sort(function (a, b) {
                              if (asc) {
                                  return (a.$[prop] > b.$[prop]) ? 1 : ((a.$[prop] < b.$[prop]) ? -1 : 0);
                              } else {
                                  return (b.$[prop] > a.$[prop]) ? 1 : ((b.$[prop] < a.$[prop]) ? -1 : 0);
                              }
                          });
                          break;
                  }
          };
      });

    hotelController.$inject = ["$scope", "$timeout", "$http"];
    function hotelController($scope, $timeout, $http) {
        $scope.mapView = false;
        $scope.toggleView = function () {
            $('<div id="popupMap" style="width:' + ($(window).width() - 100) + 'px;height:610px;" ></div>').on("shown.bs.modal", function () { 
                $timeout(function () {
                    var infoWindow, map;
                    var markers = [];

                    var mapOptions = {
                        center: new google.maps.LatLng($scope.hotels.Hotel_Response.Hotel_Details.Hotel[0].$.LAT, $scope.hotels.Hotel_Response.Hotel_Details.Hotel[0].$.LONG),
                        zoom: 12,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false
                    };

                    function initMap() {
                        if (map === void 0) {
                            map = new google.maps.Map($('#popupMap')[0], mapOptions);
                        }
                    }

                    function setMarker(map, position, title, content) {
                        var marker;
                        var markerOptions = {
                            position: position,
                            map: map,
                            title: title,
                            icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
                        };

                        marker = new google.maps.Marker(markerOptions);
                        markers.push(marker);

                        google.maps.event.addListener(marker, 'click', function () {
                            if (infoWindow !== void 0) {
                                infoWindow.close();
                            }
                            var infoWindowOptions = {
                                content: content
                            };
                            infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                            infoWindow.open(map, marker);
                        });
                    }

                    initMap();
                    angular.forEach($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (hotel) {
                        setMarker(map, new google.maps.LatLng(hotel.$.LAT, hotel.$.LONG), hotel.$.HN, '<b><a href=Detail.html?hot_id=' + hotel.$.HID + '>' + hotel.$.HN + '</a></b><br/>Address: ' + hotel.$.Add);
                    });
                }, 500);
                    
                //onClose: function () {
                //            var a = this;
                //            a.close();
                //        }

            });
           // $("#myModal").modal('show');


            $('<div id="popupMap" style="width:' + ($(window).width() - 100) + 'px;height:610px;" ></div>').modal({                
               onShow : function () {
                    $('#simplemodal-container').css('height', '610px')
                    $('#simplemodal-container').css('top', '20px')
                    $timeout(function () {
                        var infoWindow, map;
                        var markers = [];

                        var mapOptions = {
                            center: new google.maps.LatLng($scope.hotels.Hotel_Response.Hotel_Details.Hotel[0].$.LAT, $scope.hotels.Hotel_Response.Hotel_Details.Hotel[0].$.LONG),
                            zoom: 12,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            scrollwheel: false
                        };

                        function initMap() {
                            if (map === void 0) {
                                map = new google.maps.Map($('#popupMap')[0], mapOptions);
                            }
                        }

                        function setMarker(map, position, title, content) {
                            var marker;
                            var markerOptions = {
                                position: position,
                                map: map,
                                title: title,
                                icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
                            };

                            marker = new google.maps.Marker(markerOptions);
                            markers.push(marker);

                            google.maps.event.addListener(marker, 'click', function () {
                                if (infoWindow !== void 0) {
                                    infoWindow.close();
                                }
                                var infoWindowOptions = {
                                    content: content
                                };
                                infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                                infoWindow.open(map, marker);
                            });
                        }

                        initMap();
                        angular.forEach($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (hotel) {
                            setMarker(map, new google.maps.LatLng(hotel.$.LAT, hotel.$.LONG), hotel.$.HN, '<b><a href=Detail.html?hot_id=' + hotel.$.HID + '>' + hotel.$.HN + '</a></b><br/>Address: ' + hotel.$.Add);
                        });
                    }, 1000);
                },
                onClose: function () {
                    var a = this;
                    a.close();
                }
            });
        }
        $scope.orderByHotel = 'MIP';
        $scope.isAscSort = true;
        $scope.changeHotelSorting = function (fieldName) {
            if ($scope.orderByHotel == fieldName)
                $scope.isAscSort = !$scope.isAscSort;
            else
                $scope.isAscSort = true;

            $scope.orderByHotel = fieldName;
        };
        $scope.selectedPageno = 1;
        $scope.pageSize = 10;
        $scope.storeRowFrom = 0;
        $scope.storeRowTo = $scope.pageSize;
        $scope.selectPage = function (pageno) {
            $scope.selectedPageno = pageno;
            $scope.storeRowFrom = (pageno - 1) * $scope.pageSize;
            $scope.storeRowTo = pageno * $scope.pageSize;
        };
        $scope.changePage = function (pageno) {
            $scope.selectedPageno = $scope.selectedPageno + pageno;
            $scope.storeRowFrom = ($scope.selectedPageno - 1) * $scope.pageSize;
            $scope.storeRowTo = $scope.selectedPageno * $scope.pageSize;
        };
        $scope.bindCarousel = function () {
            $timeout(function () { showDivs(); }, 500);
        }
        //$scope.$watch('[hotels, filterName, filterStar, filterAccomodationType,filterMealPlan, filterGuestRating, filterAmanaties, orderByHotel, isAscSort, selectedPageno]',
        $scope.$watch(function () {
            $scope.filteredItems = $scope.$eval("hotels.Hotel_Response.Hotel_Details.Hotel | filter:filterHotel");
            if ($scope.filteredItems != null) {
                $scope.totalPage = Math.ceil($scope.filteredItems.length / $scope.pageSize);
                if ($scope.selectedPageno > $scope.totalPage) {
                    $scope.changePage(1);
                }
                $scope.bindCarousel();
            }
        });

        $scope.starRatingCount = [];
        $scope.guestRatingCount = [];
        $scope.accomodationTypeCount = [];
        $scope.mealPlanCount = [];
        $scope.amanatiesCount = [];
        $scope.loadData = function () {
            $http.get('AjaxResult?Validate=HotelResult')
                .then(function (xml) {
                    $scope.hotels = $.xml2json(xml.data);
                    $scope.hideProgressScreen();

                    t = $.xml2json(xml.data);
                    console.log($scope.hotels);

                    $scope.totalPage = Math.ceil($scope.hotels.Hotel_Response.Hotel_Details.Hotel.length / $scope.pageSize);

                    $timeout(function () {
                        $scope.starRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.STR == '1'; }).length);
                        $scope.starRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.STR == '2'; }).length);
                        $scope.starRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.STR == '3'; }).length);
                        $scope.starRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.STR == '4'; }).length);
                        $scope.starRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.STR == '5'; }).length);

                        $scope.guestRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.GT == '1'; }).length);
                        $scope.guestRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.GT == '2'; }).length);
                        $scope.guestRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.GT == '3'; }).length);
                        $scope.guestRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.GT == '4'; }).length);
                        $scope.guestRatingCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.GT == '5'; }).length);

                        $scope.accomodationTypeCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.AT == 'Hotel'; }).length);
                        $scope.accomodationTypeCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.AT == 'Serviced Apartment'; }).length);
                        $scope.accomodationTypeCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.AT == 'Hostel'; }).length);
                        $scope.accomodationTypeCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.AT == 'Guest House'; }).length);
                        $scope.accomodationTypeCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.AT == 'Apartment'; }).length);

                        $scope.mealPlanCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) {
                            var resMealPlan = true; $.each(n.Rooms.Room, function (index, room) { if (room.$ == null) resMealPlan = false; else if (resMealPlan) resMealPlan = room.$.MD.toLowerCase() == 'room only'; }); return resMealPlan;
                        }).length);
                        $scope.mealPlanCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) {
                            var resMealPlan = true; $.each(n.Rooms.Room, function (index, room) { if (room.$ == null) resMealPlan = false; else if (resMealPlan) resMealPlan = room.$.MD.toLowerCase() == 'bed and breakfast'; }); return resMealPlan;
                        }).length);
                        $scope.mealPlanCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) {
                            var resMealPlan = true; $.each(n.Rooms.Room, function (index, room) { if (room.$ == null) resMealPlan = false; else if (resMealPlan) resMealPlan = room.$.MD.toLowerCase() == 'half board'; }); return resMealPlan;
                        }).length);
                        $scope.mealPlanCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) {
                            var resMealPlan = true; $.each(n.Rooms.Room, function (index, room) { if (room.$ == null) resMealPlan = false; else if (resMealPlan) resMealPlan = room.$.MD.toLowerCase() == 'full board'; }); return resMealPlan;
                        }).length);

                        $scope.amanatiesCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.Amanaties.toLowerCase().indexOf('air cConditioning') > -1; }).length);
                        $scope.amanatiesCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.Amanaties.toLowerCase().indexOf('wi-fi') > -1; }).length);
                        $scope.amanatiesCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.Amanaties.toLowerCase().indexOf('internet') > -1; }).length);
                        $scope.amanatiesCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.Amanaties.toLowerCase().indexOf('tv') > -1; }).length);
                        $scope.amanatiesCount.push($.grep($scope.hotels.Hotel_Response.Hotel_Details.Hotel, function (n, i) { return n.$.Amanaties.toLowerCase().indexOf('washer/dryer') > -1; }).length);

                    }, 100);

                    $timeout(function () {
                        createCarouselIndex($scope.pageSize);
                        $scope.bindCarousel();
                        //jq18("#HDestTo").autocomplete("../HotelDomestic.ashx?Type=Domestic", {
                        //    delay: 5,
                        //    minChars: 3,
                        //    matchSubset: 1,
                        //    matchContains: 1,
                        //    cacheLength: 10,
                        //    selectFirst: true,
                        //    autoFill: false
                        //});
                        //var dates = jq18("#txt_ChekinDate, #txt_ChekOut").datepicker({
                        //    minDate: -0,
                        //    showOn: "both",
                        //    //buttonImage: "../../B2B_images/calander.jpg",
                        //    buttonText: ' ',
                        //    constrainInput: false,

                        //    buttonImageOnly: true,
                        //    //                  showButtonPanel: true,
                        //    numberOfMonths: 2,
                        //    dateFormat: 'dd MM yy',
                        //    beforeShow: function (input, inst) {
                        //        if (this.id == "txt_ChekinDate") {
                        //            maxDate: "+10m +1w"
                        //        }
                        //    },
                        //    onSelect: function (selectedDate) {
                        //        var option = this.id == "txt_ChekinDate" ? "minDate" : "maxDate",
                        //        instance = jq18(this).data("datepicker"),
                        //        date = jq18.datepicker.parseDate(
                        //            instance.settings.dateFormat ||
                        //            jq18.datepicker._defaults.dateFormat,
                        //            selectedDate, instance.settings);
                        //        dates.not(this).datepicker("option", option, date);

                        //        if (this.id == "txt_ChekinDate") {
                        //            var dateMax = jq18('#txt_ChekinDate').datepicker("getDate");
                        //            var rMax = new Date(dateMax.getFullYear(), dateMax.getMonth(), dateMax.getDate() + 3);
                        //            jq18('#txt_ChekOut').val(jq18.datepicker.formatDate('dd MM yy', new Date(rMax)));
                        //            calculate();
                        //        }
                        //        else {

                        //            var dateMax = jq18('#txt_ChekinDate').datepicker("getDate");
                        //            if (dateMax == null) {
                        //                var dateMin = jq18('#txt_ChekOut').datepicker("getDate");
                        //                var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() - 1);
                        //                jq18('#txt_ChekinDate').val(jq18.datepicker.formatDate('dd MM yy', new Date(rMin)));
                        //            }
                        //        }
                        //        calculate();
                        //    }
                        //});
                    }, 500);
                });

        }

        $scope.loadData();

        $scope.hideProgressScreen = function () {
            // Hide the Wait DIV.
            var waitDiv = document.getElementById("divWait");
            if (waitDiv) {
                waitDiv.style.visibility = "hidden"
                waitDiv.style.position = "absolute"
            }

            // Show the Results DIV.
            var resultsDiv = document.getElementById("wrapper");
            resultsDiv.style.display = "block"

        }

        $scope.roundPrice = function (price) {
            return Math.round(price);
        }

        $scope.show = function (id) {
            document.getElementById(id).style.display = "block";
        }
        $scope.hide = function (id) {
            document.getElementById(id).style.display = "none";
        }
        $scope.toggleVisibility = function (hotel) {
            var showbtnId = 'show' + hotel.$.HI;
            var hidebtnId = 'hide' + hotel.$.HI;

            var showbtn = document.getElementById(showbtnId);
            var hidebtn = document.getElementById(hidebtnId);
            var e = document.getElementById('rooms-toggle-' + hotel.$.HI);
            var e1 = document.getElementById('rooms-toggle1-' + hotel.$.HI);

            if (showbtn.style.display == '') {
                e.style.display = '';
                showbtn.style.display = 'none';
                hidebtn.style.display = '';
                e1.style.display = 'none';

            }
            else {
                showbtn.style.display = '';
                hidebtn.style.display = 'none';
                e.style.display = 'none';
                e1.style.display = '';

            }
        }

        $scope.bookHotel = function (hotel, room) {
            window.location = "Booking.aspx?hot_id=" + hotel.$.HID + "&RIndex=" + room.$.RI;
        }

        $scope.selectFilterStar = function (val) {
            if ($scope.filterStar == null)
                $scope.filterStar = val + ',';
            else if ($scope.filterStar.indexOf(val + ',') > -1)
                $scope.filterStar = $scope.filterStar.replace(val + ',', '');
            else
                $scope.filterStar += val + ',';
            $scope.selectedPageno = 1;
            $scope.storeRowFrom = 0;
        }
        $scope.selectFilterAmanaties = function (val) {
            val = val.toUpperCase();
            if ($scope.filterAmanaties == null)
                $scope.filterAmanaties = val + ',';
            else if ($scope.filterAmanaties.indexOf(val + ',') > -1)
                $scope.filterAmanaties = $scope.filterAmanaties.replace(val + ',', '');
            else
                $scope.filterAmanaties += val + ',';
            $scope.selectedPageno = 1;
            $scope.storeRowFrom = 0;
        }
        $scope.selectFilterAccomodationType = function (val) {
            val = val.toUpperCase();
            if ($scope.filterAccomodationType == null)
                $scope.filterAccomodationType = val + ',';
            else if ($scope.filterAccomodationType.indexOf(val + ',') > -1)
                $scope.filterAccomodationType = $scope.filterAccomodationType.replace(val + ',', '');
            else
                $scope.filterAccomodationType += val + ',';
            $scope.selectedPageno = 1;
            $scope.storeRowFrom = 0;
        }
        $scope.selectFilterMealPlan = function (val) {
            val = val.toUpperCase();
            if ($scope.filterMealPlan == null)
                $scope.filterMealPlan = val + ',';
            else if ($scope.filterMealPlan.indexOf(val + ',') > -1)
                $scope.filterMealPlan = $scope.filterMealPlan.replace(val + ',', '');
            else
                $scope.filterMealPlan += val + ',';
            $scope.selectedPageno = 1;
            $scope.storeRowFrom = 0;
        }
        $scope.selectFilterGuestRating = function (val) {
            if ($scope.filterGuestRating == null)
                $scope.filterGuestRating = val + ',';
            else if ($scope.filterGuestRating.indexOf(val + ',') > -1)
                $scope.filterGuestRating = $scope.filterGuestRating.replace(val + ',', '');
            else
                $scope.filterGuestRating += val + ',';
            $scope.selectedPageno = 1;
            $scope.storeRowFrom = 0;
        }

        $scope.selectFilterPrice = function (val) {
            var minvalue = parseInt(document.getElementById('hdnMinPrice').value, 10);
            var maxvalue = parseInt(document.getElementById('hdnMaxPrice').value, 10);
            $scope.filterPriceMin = minvalue;
            $scope.filterPriceMax = maxvalue;
        }

        $scope.filterHotel = function (item) {
            //console.log(item);
            var resName = true, resStar = true, resAccomodationType = true, resMealPlan = true, resGuestRating = true, resAmanaties = true, resPrice = true;
            if ($scope.filterName != null && $scope.filterName != '')
                resName = item.$.HN.toUpperCase().indexOf($scope.filterName.toUpperCase()) > -1;

            if ($scope.filterStar != null && $scope.filterStar != '')
                resStar = $scope.filterStar.indexOf(item.$.STR) > -1;

            if ($scope.filterAccomodationType != null && $scope.filterAccomodationType != '')
                resAccomodationType = $scope.filterAccomodationType.indexOf(item.$.AT.toUpperCase()) > -1;

            if ($scope.filterMealPlan != null && $scope.filterMealPlan != '')
                $.each(item.Rooms.Room, function (index, room) {
                    if (room.$ == null)
                        resMealPlan = false;
                    else if (resMealPlan)
                        resMealPlan = $scope.filterMealPlan.indexOf(room.$.MD.toUpperCase()) > -1;
                })

            if ($scope.filterGuestRating != null && $scope.filterGuestRating != '')
                resGuestRating = $scope.filterGuestRating.indexOf(item.$.GT) > -1;

            if (item.$.Amanaties == '')
                resAmanaties = false;
            else if ($scope.filterAmanaties != null && $scope.filterAmanaties != '')
                resAmanaties = $scope.filterAmanaties.indexOf(item.$.Amanaties.toUpperCase()) > -1;

            if (item.$.MIP != null)
                resPrice = $scope.minPrice <= parseInt(item.$.MIP, 10) && $scope.maxPrice >= parseInt(item.$.MIP, 10);

            if (resName && resStar && resAccomodationType && resMealPlan && resGuestRating && resAmanaties && resPrice)
                return true;
            else
                return false;
        }

        $scope.minPrice = 0;
        $scope.maxPrice = 50000;
        $scope.slider = {
            minValue: 0,
            maxValue: 50000,
            options: {
                floor: 0,
                ceil: 50000,
                step: 1,
                onEnd: function () {
                    $scope.minPrice = $scope.slider.minValue;
                    $scope.maxPrice = $scope.slider.maxValue;

                    console.log($scope.minPrice, $scope.maxPrice);
                }
            }
        };
        $timeout(function () { $('.rz-pointer-max').css('left', '90%'); }, 1000);
        $scope.getImageList = function (image) {
            return image.split(',');
        }

        $scope.plusDivs = function (contentIndex) {
            if ($scope.selectedPageno > 1)
                contentIndex = contentIndex - ($scope.selectedPageno - 1) * $scope.pageSize;
            myIndex[contentIndex] = (isNaN(myIndex[contentIndex]) ? 0 : myIndex[contentIndex]) + 1;
            showDivs(contentIndex);
        }

        $scope.minusDivs = function (contentIndex) {
            if ($scope.selectedPageno > 1)
                contentIndex = contentIndex - ($scope.selectedPageno - 1) * $scope.pageSize;
            myIndex[contentIndex] = (isNaN(myIndex[contentIndex]) ? 1 : myIndex[contentIndex]) - 1;
            showDivs(contentIndex);
        }

        $scope.isAmanaties = function (item, amanatiesName) {
            if (item == null || item.$.Amanaties == '')
                return false;
            else
                return item.$.Amanaties.toLowerCase().indexOf(amanatiesName.toLowerCase()) > -1;
        }
    }
})();

var myIndex = [];
function createCarouselIndex(totalCaount) {
    myIndex = [];
    for (var i = 0; i < totalCaount; i++)
        myIndex.push(0);
}
function carousel() {
    $.each($('.w3-content'), function (contentIndex, content) {
        $.each($(content).find('img.mySlides'), function (index, Item) {
            if (index != myIndex[contentIndex])
                $(Item).hide();
            else
                $(Item).show();
        });
        if (myIndex[contentIndex] == $(content).find('img.mySlides').length - 1)
            myIndex[contentIndex] = 0;
        else
            myIndex[contentIndex] = myIndex[contentIndex] + 1;
    });
    setTimeout(carousel, 5000);
}

function showDivs(n) {
    $.each($('.w3-content'), function (contentIndex, content) {
        if (contentIndex == n) {
            if (myIndex[contentIndex] < 0)
                myIndex[contentIndex] = $(content).find('img.mySlides').length - 1;
            if (myIndex[contentIndex] == $(content).find('img.mySlides').length)
                myIndex[contentIndex] = 0;
        }

        $.each($(content).find('img.mySlides'), function (index, Item) {
            if (index != myIndex[contentIndex])
                $(Item).hide();
            else
                $(Item).show();
        });
    });

}
