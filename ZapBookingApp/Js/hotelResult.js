﻿var xmlHotelResult;
var xmlOrigHotelResult;
var xmlPagerHotelResult;
var resultXml = "";
var _splitRT
var totalNoStar
var HotelStar
var sortRb = 'PR'
var decimalPref = "false"
var xu = new XML();
var dom = new DOM();
var currFactor = 1.0;
var setDisplayCurrency = false;
var TotalResultCount;
var pages;
var PagingCounter = 29;
var strOrgLocation = ',';
var scripts = new Array();
var scripts1 = new Array();
var Allmarker = new Array();
var cm_mapHTMLS = new Array();
var globalStartPos = 0;
var globalEndPos = 0;
var xmlHttp = null;
var minPrice = 10;
var maxPrice = 3000;
var currency = '';
var starCount = 1;
var priceFilter = false;
var starFilter = false;
var xmlpriceHotelResult;
var xmlstarHotelResult;
var secondPagingFlag = false;
var getresultflag = false;
var Filecounter;
var startHotelIndex = 0;
var endHotelIndex = 6;



function fnFlLink(opt) {
    debugger;
    var hdncurrentPageIndex = document.getElementById("hdnCurrentPageIndex").value;
    var currentPageIndex = parseInt(hdncurrentPageIndex);
    var nextPageIndex = parseInt(hdncurrentPageIndex) + 1;
    var prevPageIndex = parseInt(hdncurrentPageIndex) - 1;

    if (opt == "Nxt") {
        ShowPaging((nextPageIndex * PagingCounter), (nextPageIndex * PagingCounter) + PagingCounter, nextPageIndex);
        if (nextPageIndex == 7 || nextPageIndex == 13 || nextPageIndex == 19 || nextPageIndex == 25 || nextPageIndex == 31 || nextPageIndex == 37 || nextPageIndex == 43 || nextPageIndex == 49 || nextPageIndex == 55 || nextPageIndex == 61) {
            startHotelIndex = endHotelIndex;
            endHotelIndex = +endHotelIndex + 6;
            if (endHotelIndex >= pages) {
                endHotelIndex = pages;
                document.getElementById("flLink2").style.display = 'none';
                document.getElementById("flLink2Bottom").style.display = 'none';
            }
            CreatePager(TotalResultCount);
            document.getElementById("flLink").style.display = '';
            document.getElementById("flLinkBottom").style.display = '';
        }
    }
    if (opt == "Pre") {
        ShowPaging((prevPageIndex * PagingCounter), (prevPageIndex * PagingCounter) + PagingCounter, prevPageIndex);
        if (prevPageIndex == 6 || prevPageIndex == 12 || prevPageIndex == 18 || prevPageIndex == 24 || prevPageIndex == 30 || prevPageIndex == 36 || prevPageIndex == 42 || prevPageIndex == 48 || prevPageIndex == 54 || prevPageIndex == 60) {

            endHotelIndex = startHotelIndex;
            startHotelIndex -= 6;
            if (startHotelIndex < 6) {
                startHotelIndex = 0;
                endHotelIndex = startHotelIndex + 6;
                document.getElementById("flLink2").style.display = '';
                document.getElementById("flLink").style.display = 'none';
                document.getElementById("flLink2Bottom").style.display = '';
                document.getElementById("flLinkBottom").style.display = 'none';
            }
            CreatePager(TotalResultCount);
        }
    }



    //    CreatePager(TotalResultCount);

}


function fnGetResult() {
    debugger;
    var prefix = ''

    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;
        webServiceURL = "AjxHtlResult.aspx?Validate=HotelResult";
        xmlHttp.open("POST", webServiceURL, true);
        xmlHttp.onreadystatechange = fnFetchResult;
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
    }
}




function fnGetResult1(currencyname) {
    debugger;
    var prefix = ''
    var currencyname = currencyname;
    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;
        webServiceURL = "AjxHtlResult.aspx?Validate=currrency&currencyname=" + currencyname + ""
        xmlHttp.open("POST", webServiceURL, true);
        xmlHttp.onreadystatechange = fnFetchResult;
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
    }
}


function fnGetResult_Paging() {

    var prefix = ''

    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;
        webServiceURL = "AjxHtlResult.aspx?Validate=Paging"
        xmlHttp.open("GET", webServiceURL, false);
        xmlHttp.onreadystatechange = fnFetchXML;
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
    }
}


function starCheckBox(cbControl) {
    debugger;
    var chkBoxList = document.getElementsByName(cbControl);
    for (var i = 0; i < chkBoxList.length; i++) {


        chkBoxList[i].checked = false;

    }

}

function fnFetchResult() {
    debugger;
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {

        if (xmlHttp.status == 200) {

            var obdt = new Date();
            var h = obdt.getHours().toString();
            var m = obdt.getMinutes().toString();
            var s = obdt.getSeconds.toString();
            ToggleComponents();
            totalNoStar = 0;
            starCount = starCheckBox("chk_starrating");
            secondPagingFlag = false;
            var strReply = xmlHttp.responseText
            xmlHotelResult = xu.LoadXMLString(strReply);

            if (!((strReply == "") || (xmlHotelResult.firstChild.nodeName == "ErrorMessages"))) {
                var strResult;
                TotalResultCount = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@TotalCount");
                
                if (TotalResultCount == '0') {
                    TotalResultCount = new XML().GetNodeCount(xmlHotelResult, "Hotel_Details/Hotel");
                }
                if (TotalResultCount == null) {
                    TotalResultCount = new XML().GetNodeCount(xmlHotelResult, "Hotel_Details/Hotel");
                }

                xmlOrigHotelResult = xu.LoadXMLString(strReply);
                xmlPagerHotelResult = xu.LoadXMLString(strReply);

                xu.AddParameter("startIndex", "1");
                xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
                strResult = xu.XSLTransform(xmlHotelResult, "../XSLT/CommonResult.xslt", "xml");
                document.getElementById("tblResult").innerHTML = strResult;

                //                document.getElementById("lblTotal").innerHTML = TotalResultCount;\
                minPrice = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@GMin_Price");
                maxPrice = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@GMax_Price");
                currency = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@Currency");

                JquerySlider(parseInt(minPrice), parseInt(maxPrice), currency)
                document.getElementById("minAmt").value = minPrice;
                document.getElementById("maxAmt").value = maxPrice;
                document.getElementById("hdnCurrency").value = currency;
                document.getElementById("lblTotal").innerHTML = TotalResultCount;

                CreatePager(TotalResultCount);
                
                

            }
            else {

                NoResultDiv();

                // initSlider(false, minPrice, maxPrice);
                document.getElementById("Refine").style.display = 'block';
            }

        }
    }
}



function JquerySlider(minPrice, maxPrice, currency) {debugger;
    document.getElementById("hdnMinPrice").value = minPrice;
    document.getElementById("hdnMaxPrice").value = maxPrice;
    document.getElementById("hdnCurrency").value = currency;
    $("#slider-range").slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            $("#amount").val(currency + " " + ui.values[0] + " - " + currency + " " + ui.values[1]);
            document.getElementById("hdnMinPrice").value = ui.values[0];
            document.getElementById("hdnMaxPrice").value = ui.values[1];
        },
        change: setDivToggleFilterSearch
    });
    $("#amount").val(currency + " " + minPrice +
    " - " + currency + " " + maxPrice);

}

function StarRating(starCount) {

    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", minPrice);
    xu.AddParameter("maxPrice", maxPrice);
    xu.AddParameter("hotelName", '');
    var xmlResut = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");
    starFilter = true;
    xmlstarHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlstarHotelResult, "Hotel_Details/Hotel");

    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlstarHotelResult, "../XSLT/CommonResult.xslt", "xml");

    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
        //        document.getElementById("tprice").className = 'sort-tab info';
    }
    CreatePager(TotalResultCount);
}

function resultSort(sortType) {
    debugger;

    var sortDType;
    if (sortType == "MIP") {
        sortDType = "number";
    }

    else {
        sortDType = "text";
    }
    hotelResultXML_Filter(sortType, "");

}

function resultSortByHotelName(HotelName) {

    if (trim(HotelName) != "" && trim(HotelName) != null) {

        hotelResultXML_Filter("MIP", HotelName)
    }
    else {
        alert('Please fill Hotel Name')
    }
}

function CreatePager(totResults) {
    debugger;


    var localtblLength = document.getElementById("tblPaging").rows.length;
    for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
        document.getElementById('tblPaging').deleteRow(0)
    }
    localtblLength = document.getElementById("tblPagingbottom").rows.length;
    for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
        document.getElementById('tblPagingbottom').deleteRow(0)
    }

    var rowIndx = document.getElementById("tblPaging").rowIndex;
    newRow = dom.InsertTableRow("tblPaging", rowIndx + 1);

    var rowIndxbottom = document.getElementById("tblPagingbottom").rowIndex;
    var newRowbottom = dom.InsertTableRow("tblPagingbottom", rowIndxbottom + 1);
    var newCellbottom;

    var counPage = parseFloat(totResults / PagingCounter).toString().split('.')
    if (counPage.length == 2) {
        pages = parseInt(counPage[0]) + 1;
    }
    else {
        pages = counPage[0];
    }
    if (pages > endHotelIndex) {
        document.getElementById("flLink2").style.display = 'block';
        document.getElementById("flLink2Bottom").style.display = 'block';
    }
    else {
        endHotelIndex = pages;
    }




    var FirstPage = (PagingCounter * startHotelIndex) + 1;
    var NextPage;

    //    newCell = dom.InsertTableCell(newRow);
    //    newCell.className = "blackboldtxt11";
    //    newCell.width = "100%"
    //    newCell.align = "left";
    // newCell.innerHTML = "Total Results: " + totResults;
    document.getElementById("lblresult").innerHTML = totResults;

    for (var _iPage = startHotelIndex + 1; _iPage <= endHotelIndex; _iPage++) {
        newCell = dom.InsertTableCell(newRow);
        newCellbottom = dom.InsertTableCell(newRowbottom);

        NextPage = parseInt(FirstPage) + PagingCounter;
        if (_iPage != pages) {
            if (_iPage == startHotelIndex + 1) {

                newCell.innerHTML = "<a class='dark-themecurrent' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' > " + _iPage.toString() + "</span></a>"
                newCellbottom.innerHTML = "<a  class='dark-themecurrent' id='pgBottom" + _iPage.toString() + "' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' > " + _iPage.toString() + "</span></a>"
            }
            else {

                newCell.innerHTML = "<a  class='dark-theme' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' > " + _iPage.toString() + "</span></a>"
                newCellbottom.innerHTML = "<a  class='dark-theme' id='pgBottom" + _iPage.toString() + "' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' > " + _iPage.toString() + "</span></a>"

            }
        }
        else {
            var totalres = totResults + 1;
            if (_iPage == startHotelIndex + 1) {
                newCell.innerHTML = "<a  class='dark-theme' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")' style='text-decoration:none;'><span id='spnPageAll" + _iPage.toString() + "' > </span></a>&nbsp;"
                newCellbottom.innerHTML = "<a  class='dark-theme' id='pgBottom" + _iPage.toString() + "' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")' style='text-decoration:none;'><span id='spnPageAllbottom" + _iPage.toString() + "' > </span></a>&nbsp;"
            }
            else {
                newCell.innerHTML = "<a  class='dark-theme' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' > " + _iPage.toString() + "</span></a>&nbsp;"
                newCellbottom.innerHTML = "<a  class='dark-theme' id='pgBottom" + _iPage.toString() + "' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' > " + _iPage.toString() + "</span></a>&nbsp;"
            }
        }
        FirstPage = parseInt(FirstPage) + PagingCounter;
    }
}

function ShowPaging(startpos, endpos, currposCount) {
    debugger;
    document.getElementById("hdnCurrentPageIndex").value = currposCount;
    ShowPagingMain(startpos, endpos, currposCount);
       globalStartPos = startpos;
        globalEndPos = endpos;
}

function ShowPagingMain(start, end, posCount) {
    debugger;
    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    for (var _iPage = 1; _iPage <= pages; _iPage++) {
        if (document.getElementById("spnPageAll" + _iPage)) {
            if (_iPage == posCount) {
                document.getElementById("pg" + _iPage).className = "dark-themecurrent";
                document.getElementById("pgBottom" + _iPage).className = "dark-themecurrent";

            }
            else {
                document.getElementById("pg" + _iPage).className = "dark-theme";
                document.getElementById("pgBottom" + _iPage).className = "dark-theme";

            }
        }
    }

    xu = new XML();
    xu.AddParameter("startIndex", start);
    xu.AddParameter("endIndex", end);


    /////////////////////WEBSERVICE CONCEPT?????????????????
    if (secondPagingFlag == false) {
        var str = GetCacheContent_2();
    }


    if (priceFilter == true) {
        strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "html");

    }
    else if (starFilter == true) {
        strResult = xu.XSLTransform(xmlstarHotelResult, "../XSLT/CommonResult.xslt", "html");

    }
    else {
        strResult = xu.XSLTransform(xmlPagerHotelResult, "../XSLT/CommonResult.xslt", "html");
    }

    xmlHotelResult = xmlPagerHotelResult;
    document.getElementById("tblResult").innerHTML = strResult;
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
}

function ToggleComponents() {
    // Hide the Wait DIV.
    var waitDiv = document.getElementById("divWait");
    if (waitDiv) {
        waitDiv.style.visibility = "hidden"
        waitDiv.style.position = "absolute"
    }

    // Show the Results DIV.
    var resultsDiv = document.getElementById("wrapper");
    resultsDiv.style.display = "block"

}

function setDivToggleFilterSearch() {
    window.setTimeout(hotelResultXML, 1);
    //ToggleLayer(true);
}


function hotelResultGuestRating() {
    debugger;
    var gusetrating;
    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    var sortType = "MIP";
    if (sortType != "MIP") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }

    minPrice = document.getElementById("hdnMinPrice").value;
    maxPrice = document.getElementById("hdnMaxPrice").value;
    starCount = CheckboxGuestList("chk_GuestRating");
    gusetrating = CheckboxGuestList("chk_GuestRating");

    xu.AddParameter("starRating", starCount);
    xu.AddParameter("guestRating", gusetrating);
    xu.AddParameter("minPrice", parseInt(minPrice) - 1);
    xu.AddParameter("maxPrice", parseInt(maxPrice) + 1);

    var HotelName = document.getElementById("Refine_hotelName").value;
    if (trim(HotelName) != "" && trim(HotelName) != null) {
        xu.AddParameter("hotelName", HotelName);
    }
    else {
        xu.AddParameter("hotelName", '');
    }

    var e = document.getElementById("cph_main_Refine_drphtlaminities");
    HotelAminites = e.options[e.selectedIndex].value;
    if (trim(HotelAminites) != "" && trim(HotelAminites) != null) {
        xu.AddParameter("hotelaminites", HotelAminites);
    }
    else {
        xu.AddParameter("hotelaminites", '');
    }

    var HotelLocation = document.getElementById("Refine_hotellocation").value;
    if (trim(HotelLocation) != "" && trim(HotelLocation) != null) {
        xu.AddParameter("hotelLocation", HotelLocation);
    }
    else {
        xu.AddParameter("hotelLocation", '');
    }

    //////////New Concept for speed START //////

    if (secondPagingFlag == false) {
        var str = GetCacheContent_2();
    }


    /////////New Concept for speed END ////////



    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");
    xmlResult = xu.LoadXMLString(xmlResult);

    starCount = CheckboxList("chk_GuestRating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("guestRating", gusetrating);
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("hotelName", '');
    xu.AddParameter("hotelLocation", '');
    xu.AddParameter("hotelaminites", '');
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/HotelFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "Hotel_Details/Hotel");




    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "xml");
    //document.getElementById("tblResult").innerHTML = strResult;
    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        //        document.getElementById('Refine_hotellocation').value = "";

        //        var e1 = document.getElementById("cph_main_Refine_drphtlaminities");
        //         e1.options[e.selectedIndex].value="";

        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
        //        document.getElementById("tprice").className = 'sort-tab info';
    }
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
    CreatePager(TotalResultCount);
   
}


function hotelResultXMLAcc() {
    debugger;
    var hotelcat = ' ';
    var mealpan;
    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    var sortType = "MIP";
    if (sortType != "MIP") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }

  
   
    minPrice = document.getElementById("hdnMinPrice").value;
    maxPrice = document.getElementById("hdnMaxPrice").value;
    starCount = CheckboxList("chk_starrating");
    hotelcat = CheckboxListAccomadtion("chk_AccomodationType");
    mealpan = CheckboxListMeal("chk_Mealplan");
    amenities = CheckboxListMeal("chk_hAmenities");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("mealpaln", mealpan);
    xu.AddParameter("hotelcat", hotelcat);
    xu.AddParameter("minPrice", parseInt(minPrice) - 1);
    xu.AddParameter("maxPrice", parseInt(maxPrice) + 1);
    xu.AddParameter("hotelcat", hotelcat);
   

    //////////New Concept for speed START //////

//    if (secondPagingFlag == false) {
//        var str = GetCacheContent_2();
//    }


    /////////New Concept for speed END ////////



    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelTypeFilter.xslt", "xml");
    xmlResult = xu.LoadXMLString(xmlResult);

    starCount = CheckboxList("chk_starrating");
    guestCount = CheckboxGuestList("chk_GuestRating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("guestRating", guestCount);
    xu.AddParameter("hotelcat", hotelcat);
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("hotelName", '');
    xu.AddParameter("hotelLocation", '');
    xu.AddParameter("hotelaminites", '');
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/HotelFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "Hotel_Details/Hotel");




    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "xml");
    //document.getElementById("tblResult").innerHTML = strResult;
    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        //        document.getElementById('Refine_hotellocation').value = "";

        //        var e1 = document.getElementById("cph_main_Refine_drphtlaminities");
        //         e1.options[e.selectedIndex].value="";

        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
        //        document.getElementById("tprice").className = 'sort-tab info';
    }
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
    CreatePager(TotalResultCount);
  
}


function hotelResultXMLRoomType() {
    debugger;
    var hotelcat = ' ';
    var mealpan;
    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    var sortType = "MIP";
    if (sortType != "MIP") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }


    minPrice = document.getElementById("hdnMinPrice").value;
    maxPrice = document.getElementById("hdnMaxPrice").value;
    starCount = CheckboxList("chk_starrating");
    hotelcat = CheckboxListAccomadtion("chk_AccomodationType");
    mealpan = CheckboxListMeal("chk_Mealplan");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("mealpaln", mealpan);
    xu.AddParameter("hotelcat", hotelcat);
    xu.AddParameter("minPrice", parseInt(minPrice) - 1);
    xu.AddParameter("maxPrice", parseInt(maxPrice) + 1);

    var HotelName = document.getElementById("Refine_hotelName").value;
    if (trim(HotelName) != "" && trim(HotelName) != null) {
        xu.AddParameter("hotelName", HotelName);
    }
    else {
        xu.AddParameter("hotelName", '');
    }

    var e = document.getElementById("cph_main_Refine_drphtlaminities");
    HotelAminites = e.options[e.selectedIndex].value;
    if (trim(HotelAminites) != "" && trim(HotelAminites) != null) {
        xu.AddParameter("hotelaminites", HotelAminites);
    }
    else {
        xu.AddParameter("hotelaminites", '');
    }

    var HotelLocation = document.getElementById("Refine_hotellocation").value;
    if (trim(HotelLocation) != "" && trim(HotelLocation) != null) {
        xu.AddParameter("hotelLocation", HotelLocation);
    }
    else {
        xu.AddParameter("hotelLocation", '');
    }

    //////////New Concept for speed START //////


//   var str = GetCacheContent_roomtype(mealpan);
   


    /////////New Concept for speed END ////////



   var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");
    xmlResult = xu.LoadXMLString(xmlResult);

    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("mealpaln", mealpan);   
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("hotelName", '');
    xu.AddParameter("hotelLocation", '');
    xu.AddParameter("hotelaminites", '');
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/HotelFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "Hotel_Details/Hotel");




    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "xml");
    //document.getElementById("tblResult").innerHTML = strResult;
    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        //        document.getElementById('Refine_hotellocation').value = "";

        //        var e1 = document.getElementById("cph_main_Refine_drphtlaminities");
        //         e1.options[e.selectedIndex].value="";

        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
        //        document.getElementById("tprice").className = 'sort-tab info';
    }
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
    CreatePager(TotalResultCount);

}


function hotelResultXML() {
    debugger;
    var hotelcat = ' ';
    var mealpan = '';
    var amanities = '';
    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    var sortType = "MIP";
    if (sortType != "MIP") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }

    minPrice = document.getElementById("hdnMinPrice").value;
    maxPrice = document.getElementById("hdnMaxPrice").value;
    starCount =   CheckboxList("chk_starrating");
    hotelcat = CheckboxListAccomadtion("chk_AccomodationType");
    mealpan = CheckboxListMeal("chk_Mealplan");
    amanities = CheckboxListAmanities("chk_hAmenities");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("mealpaln", mealpan);
    xu.AddParameter("hotelcat", hotelcat);
    xu.AddParameter("amanities", amanities);
    xu.AddParameter("minPrice", parseInt(minPrice) - 1);
    xu.AddParameter("maxPrice", parseInt(maxPrice) + 1);

    var HotelName = document.getElementById("Refine_hotelName").value;
    if (trim(HotelName) != "" && trim(HotelName) != null) {
        xu.AddParameter("hotelName", HotelName);
    }
    else {
        xu.AddParameter("hotelName", '');
    }
  
//    var e = document.getElementById("cph_main_Refine_drphtlaminities");
//    HotelAminites = e.options[e.selectedIndex].value;
//    if (trim(HotelAminites) != "" && trim(HotelAminites) != null) {
//        xu.AddParameter("hotelaminites", HotelAminites);
//    }
//    else {
//        xu.AddParameter("hotelaminites", '');
//    }

//    var HotelLocation = document.getElementById("Refine_hotellocation").value;
//    if (trim(HotelLocation) != "" && trim(HotelLocation) != null) {
//        xu.AddParameter("hotelLocation", HotelLocation);
//    }
//    else {
//        xu.AddParameter("hotelLocation", '');
//    }

    //////////New Concept for speed START //////

    if (secondPagingFlag == false) {
        var str = GetCacheContent_2();
    }


    /////////New Concept for speed END ////////



    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");
    xmlResult = xu.LoadXMLString(xmlResult);

    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("mealpaln", mealpan);
    xu.AddParameter("hotelcat", hotelcat);
    xu.AddParameter("amanities", amanities);
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("hotelName", '');
    xu.AddParameter("hotelLocation", '');
    xu.AddParameter("hotelaminites", '');
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/HotelFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "Hotel_Details/Hotel");




    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "xml");
    //document.getElementById("tblResult").innerHTML = strResult;
    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        //        document.getElementById('Refine_hotellocation').value = "";

        //        var e1 = document.getElementById("cph_main_Refine_drphtlaminities");
        //         e1.options[e.selectedIndex].value="";

        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
        //        document.getElementById("tprice").className = 'sort-tab info';
    }
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
    CreatePager(TotalResultCount);
   
}

function hotelResultXML_Filter(sortType, HotelName) {

    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    var sortType;
    if (sortType != "MIP") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }


    minPrice = document.getElementById("hdnMinPrice").value;
    maxPrice = document.getElementById("hdnMaxPrice").value;
    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", minPrice - 1);
    xu.AddParameter("maxPrice", maxPrice + 1);
    if (trim(HotelName) != "" && trim(HotelName) != null) {
        xu.AddParameter("hotelName", HotelName);
    }
    else {
        xu.AddParameter("hotelName", '');
    }

    //////////New Concept for speed START //////

    if (secondPagingFlag == false) {
        var str = GetCacheContent_2();
    }


    /////////New Concept for speed END ////////

    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");

    xmlResult = xu.LoadXMLString(xmlResult);

    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("hotelName", '');

    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/HotelFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "Hotel_Details/Hotel");
    document.getElementById('lblTotal').value = TotalResultCount;
    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "xml");
    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";

        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
        if (sortType == "MIP") {
            //            document.getElementById("tprice").className = 'sort-tab';
            //            document.getElementById("tname").className = 'tabs';
            //            document.getElementById("trating").className = 'tabs';
        }
        else if (sortType == "HN") {
            //        document.getElementById("tprice").className = 'tabs';
            //        document.getElementById("tname").className = 'sort-tab';
            //        document.getElementById("trating").className = 'tabs';
        }
        else {
            //            document.getElementById("tprice").className = 'tabs';
            //            document.getElementById("tname").className = 'tabs';
            //            document.getElementById("trating").className = 'sort-tab';
        }
    }
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
    CreatePager(TotalResultCount);
  
}


function classFilter(r, m, not) {
    m = " " + m + " ";
    var tmp = [];
    for (var i = 0; r[i]; i++) {
        var pass = (" " + r[i].className + " ").indexOf(m) >= 0;
        if (not ^ pass)
            tmp.push(r[i]);
    }
    return tmp;
}


function trim(str) {
    if (!str || typeof str != 'string')
        return null;

    return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
}




//function fillLocation(strResult) {
//    alert(document.getElementById("ddlLocation"));

//    xu = new XML();
//    var xmlLocation;
//    xmlLocation = xu.LoadXMLString(strResult);
//    var ddlValue = document.getElementById('ddlLocation');
//    var countPage = new XML().GetNodeCount(xmlLocation, "Hotel_Details/Hotel/@Location");
//    ddlValue.options[0] = new Option("--select--", "--select--");
//    for (var i = 1; i <= countPage; i++) {
//        var textField = i;
//        var valueField = new XML().SelectNodeValue(xmlLocation, "Hotel_Details/Hotel/@Location[" + i + "]");
//        if (valueField != '' && valueField != null) {
//            ddlValue.options[i] = new Option(valueField, valueField);
//            strOrgLocation += valueField + ','
//        }
//        else
//            strOrgLocation += ',' + ','
//    }

//}

function NoResultDiv() {
    // Hide the Wait DIV.

    var noResultDiv = document.getElementById("noResult");
    noResultDiv.style.display = "block"

}


function CheckboxList(cbControl) {
    var flag = "false";
    var startVal = "";
    var chkBoxList = document.getElementsByName(cbControl);
    for (var i = 0; i < chkBoxList.length; i++) {
        if (chkBoxList[i].checked) {
            if (flag == "false") {
                startVal = chkBoxList[i].value;
                flag = "true";
            }
            else {
                startVal = startVal + "," + chkBoxList[i].value;
            }
        }
    }

    if (startVal == "") {

        startVal = "1,2,3,4,5";

    }
    return startVal;
}


function CheckboxListMeal(cbControl) {
    var flag = "false";
    var startVal = "";
    var chkBoxList = document.getElementsByName(cbControl);
    for (var i = 0; i < chkBoxList.length; i++) {
        if (chkBoxList[i].checked) {
            if (flag == "false") {
                startVal = chkBoxList[i].value;
                flag = "true";
            }
            else {
                startVal = startVal + "," + chkBoxList[i].value;
            }
        }
    }

    if (startVal == "") {

        startVal = "BED AND BREAKFAST,Half board,Full board,ROOM ONLY";

    }
    return startVal;
}

function CheckboxListAccomadtion(cbControl) {
    var flag = "false";
    var startVal = "";
    var chkBoxList = document.getElementsByName(cbControl);
    for (var i = 0; i < chkBoxList.length; i++) {
        if (chkBoxList[i].checked) {
            if (flag == "false") {
                startVal = chkBoxList[i].value;
                flag = "true";
            }
            else {
                startVal = startVal + "," + chkBoxList[i].value;
            }
        }
    }

    if (startVal == "") {

        startVal = "Guest House,Apartment,Serviced Apartment,Hostel,Hotel";

    }
    return startVal;
}

function CheckboxGuestList(cbControl) {
    var flag = "false";
    var startVal = "";
    var chkBoxList = document.getElementsByName(cbControl);
    for (var i = 0; i < chkBoxList.length; i++) {
        if (chkBoxList[i].checked) {
            if (flag == "false") {
                startVal = chkBoxList[i].value;
                flag = "true";
            }
            else {
                startVal = startVal + "," + chkBoxList[i].value;
            }
        }
    }

    if (startVal == "") {

        startVal = "Hotel,Guest House,3,4,5";

    }
    return startVal;
}

function CheckboxListAmanities(cbControl) {
    var flag = "false";
    var startVal = "";
    var chkBoxList = document.getElementsByName(cbControl);
    for (var i = 0; i < chkBoxList.length; i++) {
        if (chkBoxList[i].checked) {
            if (flag == "false") {
                startVal = chkBoxList[i].value;
                flag = "true";
            }
            else {
                startVal = startVal + "," + chkBoxList[i].value;
            }
        }
    }

    if (startVal == "") {

        startVal = "Air Conditioning,Wi-Fi,Internet,TV,Washer/Dryer";

    }
    return startVal;
}


function GetCacheContent_2() {
    debugger;

    var prefix = ''

    var filename = document.getElementById("cph_main_hdnfile").value + "$" + document.getElementById("cph_main_fileCounter").value;
    xmlHttp = null;
    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;


        webServiceURL = "AjxHtlResult.aspx?Validate=Paging&FileName=" + filename + "";


        //alert(filename);

        xmlHttp.open("GET", webServiceURL, false);
        //xmlHttp.onreadystatechange = fnFetchXML;
        // alert(xmlHttp.readyState);
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
        fnFetchXML();

    }
}


function GetCacheContent_roomtype(roomtype) {
    debugger;

    var prefix = ''

    var filename = document.getElementById("cph_main_hdnfile").value + "$" + document.getElementById("cph_main_fileCounter").value;
    xmlHttp = null;
    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;


        webServiceURL = "AjxHtlResult.aspx?Validate=Paging&FileName=" + filename + "&roomtype=" + roomtype;


        //alert(filename);

        xmlHttp.open("GET", webServiceURL, false);
        //xmlHttp.onreadystatechange = fnFetchXML;
        // alert(xmlHttp.readyState);
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
        fnFetchXML();

    }
}


function fnFetchXML() {

    debugger;
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {

        if (xmlHttp.status == 200) {

            var strReply = xmlHttp.responseText

            //alert(strReply);

            //            strReply = strReply.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
            //            strReply = strReply.replace("<string xmlns=\"http://tempuri.org/\">", "");
            //            strReply = strReply.replace(/\&lt;/g, "<")
            //            strReply = strReply.replace("</string>", "");
            //            strReply = strReply.replace(/\&gt;/g, ">")
            //            strReply = trim(strReply);

            getresultflag = true;
            xmlHotelResult = xu.LoadXMLString(strReply);
            if (!((strReply == ""))) {
                var strResult;
                xmlOrigHotelResult = xu.LoadXMLString(strReply);
                xmlPagerHotelResult = xu.LoadXMLString(strReply);

                secondPagingFlag = true;
            }


        }

    }
}

function trim(str) {
    if (!str || typeof str != 'string')
        return null;

    return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
}


function CreateHtlNameFilter(txtHotelName) {
    debugger;

    var HotelName = document.getElementById(txtHotelName).value;
    if (HotelName != null && HotelName != "") {
        // document.getElementById("lblHtlName").innerHTML = HotelName;
        //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        hotelResultXML();
    }
    else
        alert('Please fill Hotel Name');
}
function CreateHtlLocationFilter(txtHotellocation) {
    debugger;

    var HotelLocation = document.getElementById(txtHotellocation).value;
    if (HotelLocation != null && HotelLocation != "") {
        // document.getElementById("lblHtlName").innerHTML = HotelName;
        //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        hotelResultXML();
    }
    else
        alert('Please fill Hotel Location');
}
function CreateHtlLocationFilter(txtHotellocation) {
    debugger;

    var HotelLocation = document.getElementById(txtHotellocation).value;
    if (HotelLocation != null && HotelLocation != "") {
        // document.getElementById("lblHtlName").innerHTML = HotelName;
        //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        hotelResultXML();
    }
    else
        alert('Please fill Hotel Location');
}

function CreateHtlAminitesFilter() {
    debugger;

    var e = document.getElementById("cph_main_Refine_drphtlaminities");
    HotelAminites = e.options[e.selectedIndex].value;
    if (HotelAminites != null && HotelAminites != "" && HotelAminites != "0") {
        // document.getElementById("lblHtlName").innerHTML = HotelName;
        //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        hotelResultXML();
    }
    else
        alert('Please fill Hotel Aminities');
}


function ClearHtlNameFilter() {
    // document.getElementById("lblHtlName").innerHTML = "";
    document.getElementById("Refine_hotelName").value = "";
    //  document.getElementById("trHtlNameFliter").style.display = "none";
    hotelResultXML();
}
function ClearHtlLocationFilter() {
    // document.getElementById("lblHtlName").innerHTML = "";
    document.getElementById("Refine_hotellocation").value = "";
    //  document.getElementById("trHtlNameFliter").style.display = "none";
    hotelResultXML();
}

function ClearStarFilter() {
    var chkBoxList = document.getElementsByName("chk_starrating");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = false;
    }
    hotelResultXML();
}
function SelectStarFilter() {
    var chkBoxList = document.getElementsByName("chk_starrating");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = true;
    }
    hotelResultXML();
}

function ClearStarFilter() {
    var chkBoxList = document.getElementsByName("chk_starrating");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = false;
    }
    hotelResultXML();
}

function SelectRoomFilter() {
    var chkBoxList = document.getElementsByName("chk_Mealplan");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = true;
    }
    hotelResultXML();
}

function ClearRoomFilter() {
    var chkBoxList = document.getElementsByName("chk_Mealplan");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = false;
    }
    hotelResultXML();
}

function SelectAmenitiesFilter() {
    var chkBoxList = document.getElementsByName("chk_hAmenities");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = true;
    }
    hotelResultXML();
}
function ClearAmenitiesFilter() {

    var chkBoxList = document.getElementsByName("chk_hAmenities");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = false;
    }

    document.getElementById("Refine_hotelName").value = "";

    minPrice = document.getElementById("minAmt").value;
    maxPrice = document.getElementById("maxAmt").value;
    currency = document.getElementById("hdnCurrency").value;
    JquerySlider(parseInt(minPrice), parseInt(maxPrice), currency)

    hotelResultXML();
}

function show(div_price) {
    document.getElementById(div_price).style.display = "block";
}
function hide(div_price) {

    document.getElementById(div_price).style.display = "none";
}


     
        function datediff() {
            var d1 = $('#txt_ChekinDate').datepicker("getDate");
            var d2 = $('#txt_ChekOut').datepicker("getDate");

            var diff = 1;
            if (d1 && d2) {
                var timeDiff = Math.abs(d2.getTime() - d1.getTime());
                diff = Math.ceil(timeDiff / (1000 * 3600 * 24));

            }

            $('#cbo_Night').val(diff);


        }
    
 