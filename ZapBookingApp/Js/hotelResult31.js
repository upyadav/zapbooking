﻿var xmlHotelResult;
var xmlOrigHotelResult;
var xmlPagerHotelResult;
var resultXml = "";
var _splitRT
var totalNoStar
var HotelStar
var sortRb = 'PR'
var decimalPref = "false"
var xu = new XML();
var dom = new DOM();
var currFactor = 1.0;
var setDisplayCurrency = false;
var TotalResultCount;
var pages;
var PagingCounter = 29;
var strOrgLocation = ',';
var scripts = new Array();
var scripts1 = new Array();
var Allmarker = new Array();
var cm_mapHTMLS = new Array();
var globalStartPos = 0;
var globalEndPos = 0;
var xmlHttp = null;
var minPrice = 10;
var maxPrice = 3000;
var currency = '';
var starCount = 1;
var priceFilter = false;
var starFilter = false;
var xmlpriceHotelResult;
var xmlstarHotelResult;
var secondPagingFlag = false;
var getresultflag = false;
var Filecounter;


function fnGetResult() {

    var prefix = ''

    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;
        webServiceURL = "AjxHtlResult.aspx?Validate=HotelResult"
        xmlHttp.open("POST", webServiceURL, true);
        xmlHttp.onreadystatechange = fnFetchResult;
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
    }
}


function fnGetResult_Paging() {

    var prefix = ''

    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;
        webServiceURL = "AjxHtlResult.aspx?Validate=Paging"
        xmlHttp.open("GET", webServiceURL, false);
        xmlHttp.onreadystatechange = fnFetchXML;
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
    }
}

function fnFetchResult() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {

        if (xmlHttp.status == 200) {

            var obdt = new Date();
            var h = obdt.getHours().toString();
            var m = obdt.getMinutes().toString();
            var s = obdt.getSeconds.toString();          
            ToggleComponents();
            secondPagingFlag = false;
            var strReply = xmlHttp.responseText
            xmlHotelResult = xu.LoadXMLString(strReply);

            if (!((strReply == "") || (xmlHotelResult.firstChild.nodeName == "ErrorMessages"))) {
                var strResult;
                TotalResultCount = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@TotalCount");
                document.getElementById("lblTotal").innerHTML = TotalResultCount;
                if (TotalResultCount == '0') {
                    TotalResultCount = new XML().GetNodeCount(xmlHotelResult, "Hotel_Details/Hotel");
                }
                if (TotalResultCount == null) {
                    TotalResultCount = new XML().GetNodeCount(xmlHotelResult, "Hotel_Details/Hotel");
                }
                
                xmlOrigHotelResult = xu.LoadXMLString(strReply);
                xmlPagerHotelResult = xu.LoadXMLString(strReply);
                
                xu.AddParameter("startIndex", "1");
                xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
                strResult = xu.XSLTransform(xmlHotelResult, "../XSLT/CommonResult.xslt", "xml");
                document.getElementById("tblResult").innerHTML = strResult;
                CreatePager(TotalResultCount);
                minPrice = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@GMin_Price");
                maxPrice = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@GMax_Price");
                currency = new XML().SelectNodeValue(xmlHotelResult, "/Hotel_Response/Master/@Currency");
               
                JquerySlider(parseInt(minPrice), parseInt(maxPrice), currency)
                document.getElementById("minAmt").value = minPrice;
                document.getElementById("maxAmt").value = maxPrice;
                document.getElementById("hdnCurrency").value = currency;

            }
            else {
                NoResultDiv();
               
               // initSlider(false, minPrice, maxPrice);
                document.getElementById("Refine").style.display = 'none';
            }

        }
    }
}

function JquerySlider(minPrice, maxPrice,currency) {
    document.getElementById("hdnMinPrice").value = minPrice;
    document.getElementById("hdnMaxPrice").value = maxPrice;
    document.getElementById("hdnCurrency").value = currency;
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            $("#amount").val(currency + " " + ui.values[0] + " - " + currency + " " + ui.values[1]);
            document.getElementById("hdnMinPrice").value = ui.values[0];
            document.getElementById("hdnMaxPrice").value = ui.values[1];
        },
        change: setDivToggleFilterSearch
    });
    $("#amount").val(currency + " " + minPrice +
    " - " + currency + " " + maxPrice);
   
}

function StarRating(starCount) {

    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", minPrice);
    xu.AddParameter("maxPrice", maxPrice);
    xu.AddParameter("hotelName", '');
    var xmlResut = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");
    starFilter = true;
    xmlstarHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlstarHotelResult, "Hotel_Details/Hotel");
   
    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlstarHotelResult, "../XSLT/CommonResult.xslt", "xml");

    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
//        document.getElementById("tprice").className = 'sort-tab info';
    }
    CreatePager(TotalResultCount);
}

function resultSort(sortType) {

    var sortDType;
    if (sortType == "MIP") {
        sortDType = "number";
    }
    
    else {
        sortDType = "text";
    }
    hotelResultXML_Filter(sortType, "");

}

function resultSortByHotelName(HotelName) {

    if (trim(HotelName) != "" && trim(HotelName) != null) {

        hotelResultXML_Filter("MIP", HotelName)
    }
    else {
        alert('Please fill Hotel Name')
    }
}

function CreatePager(totResults) {


    var localtblLength = document.getElementById("tblPaging").rows.length
    for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
        document.getElementById('tblPaging').deleteRow(0)
    }
    localtblLength = document.getElementById("tblPagingbottom").rows.length
    for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
        document.getElementById('tblPagingbottom').deleteRow(0)
    }

    var rowIndx = document.getElementById("tblPaging").rowIndex;
    newRow = dom.InsertTableRow("tblPaging", rowIndx + 1);

    var rowIndxbottom = document.getElementById("tblPagingbottom").rowIndex;
    var newRowbottom = dom.InsertTableRow("tblPagingbottom", rowIndxbottom + 1);
    var newCellbottom;

    var counPage = parseFloat(totResults / PagingCounter).toString().split('.')
    if (counPage.length == 2) {
        pages = parseInt(counPage[0]) + 1;
    }
    else {
        pages = counPage[0];
    }
    var FirstPage = 1;
    var NextPage;

    newCell = dom.InsertTableCell(newRow);
    newCell.className = "blackboldtxt11";
    newCell.width = "100%"
    newCell.align = "left";
   // newCell.innerHTML = "Total Results: " + totResults;
    document.getElementById("lblresult").innerHTML = totResults;
    for (var _iPage = 1; _iPage <= pages; _iPage++) {
        newCell = dom.InsertTableCell(newRow);
        newCellbottom = dom.InsertTableCell(newRowbottom);

        NextPage = parseInt(FirstPage) + PagingCounter;
        if (_iPage != pages) {
            if (_iPage == 1) {

                newCell.innerHTML = "<a id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' style='color:Red; cursor:pointer; background:#fff; padding:2px 5px; font-weight:bold;text-decoration:none;'> " + _iPage.toString() + "</span></a>&nbsp;|"
                newCellbottom.innerHTML = "<a href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:black; cursor:pointer; font-weight:bold;text-decoration:none;'> " + _iPage.toString() + "</span></a>&nbsp;|"
            }
            else {
                newCell.innerHTML = "<a id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' style='color:#7d7d7d;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;|"
                newCellbottom.innerHTML = "<a href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:blue;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;|"
            }
        }
        else {
            var totalres = totResults + 1;
            if (_iPage == 1) {
                newCell.innerHTML = "<a id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")' style='text-decoration:none;'><span id='spnPageAll" + _iPage.toString() + "' style='color:black; cursor:pointer; font-weight:bold;text-decoration:none;'> </span></a>&nbsp;"
                newCellbottom.innerHTML = "<a href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")' style='text-decoration:none;'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:black; cursor:pointer; font-weight:bold;text-decoration:none;'> </span></a>&nbsp;"
            }
            else {
                newCell.innerHTML = "<a id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' style='color:#7d7d7d;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;"
                newCellbottom.innerHTML = "<a href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging(" + FirstPage + "," + totalres + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:blue;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;"
            }
        }
        FirstPage = parseInt(FirstPage) + PagingCounter;
    }
}

function ShowPaging(startpos, endpos, currposCount) {

    ShowPagingMain(startpos, endpos, currposCount);
    globalStartPos = startpos;
    globalEndPos = endpos;
}

function ShowPagingMain(start, end, posCount) {
    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    for (var _iPage = 1; _iPage <= pages; _iPage++) {
        if (document.getElementById("spnPageAll" + _iPage)) {
            if (_iPage == posCount) {
                document.getElementById("spnPageAll" + _iPage).style.color = 'Red';
                document.getElementById("spnPageAll" + _iPage).style.fontWeight = "bold";
                document.getElementById("spnPageAll" + _iPage).style.fontSize = "19";
                document.getElementById("spnPageAll" + _iPage).style.background="#fff";
                document.getElementById("spnPageAllbottom" + _iPage).style.color = 'Red';
                document.getElementById("spnPageAllbottom" + _iPage).style.fontWeight = "bold"
                document.getElementById("spnPageAllbottom" + _iPage).style.fontSize = "19";;
                document.getElementById("spnPageAllbottom" + _iPage).style.background="#fff";
                
            }
            else {
                document.getElementById("spnPageAll" + _iPage).style.color = 'black';
                document.getElementById("spnPageAll" + _iPage).style.fontWeight = "normal";
                document.getElementById("spnPageAll" + _iPage).style.background="none";
                document.getElementById("spnPageAll" + _iPage).style.fontSize = "";
                document.getElementById("spnPageAllbottom" + _iPage).style.color = 'black';
                document.getElementById("spnPageAllbottom" + _iPage).style.fontWeight = "normal";
                document.getElementById("spnPageAllbottom" + _iPage).style.background="none";
                 document.getElementById("spnPageAllbottom" + _iPage).style.fontSize = "";
            }
        }
    }

    xu = new XML();
    xu.AddParameter("startIndex", start);
    xu.AddParameter("endIndex", end);


/////////////////////WEBSERVICE CONCEPT?????????????????
if (secondPagingFlag==false)
{
    var str = GetCacheContent_2();
  }
 

      if (priceFilter == true) {
           strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "html");

       }
       else if (starFilter == true) {
           strResult = xu.XSLTransform(xmlstarHotelResult, "../XSLT/CommonResult.xslt", "html");

      }
    else {
        strResult = xu.XSLTransform(xmlPagerHotelResult, "../XSLT/CommonResult.xslt", "html");
      }

        xmlHotelResult = xmlPagerHotelResult;
       document.getElementById("tblResult").innerHTML = strResult;
       $('#waitScreen').css({ 'display': 'none' });
       $('#waitBox').css({ 'display': 'none' });
}

function ToggleComponents() {
    // Hide the Wait DIV.
    var waitDiv = document.getElementById("divWait");
    if (waitDiv) {
        waitDiv.style.visibility = "hidden"
        waitDiv.style.position = "absolute"
    }

    // Show the Results DIV.
    var resultsDiv = document.getElementById("wrapper");
    resultsDiv.style.display = "block"

}

function setDivToggleFilterSearch() {
    window.setTimeout(hotelResultXML, 1);
    //ToggleLayer(true);
}


function hotelResultXML() {debugger;

    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    var sortType = "MIP";
    if (sortType != "MIP") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }
    
    minPrice = document.getElementById("hdnMinPrice").value;
    maxPrice = document.getElementById("hdnMaxPrice").value;
    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", parseInt(minPrice) - 1);
    xu.AddParameter("maxPrice", parseInt(maxPrice) + 1);

    var HotelName = document.getElementById("Refine_hotelName").value;
    if (trim(HotelName) != "" && trim(HotelName) != null) {
        xu.AddParameter("hotelName", HotelName);
    }
    else {
        xu.AddParameter("hotelName", '');
    }
    var HotelLocation = document.getElementById("Refine_hotellocation").value;
    if (trim(HotelLocation) != "" && trim(HotelLocation) != null) {
        xu.AddParameter("hotelLocation", HotelLocation);
    }
    else {
        xu.AddParameter("hotelLocation", '');
    }
//    var Meal_Des = document.getElementById("Refine_hotelmealplan").value;
//    if (trim(Meal_Des) != "" && trim(Meal_Des) != null) {
//        xu.AddParameter("meal_des", Meal_Des);
//    }
//    else {
//        xu.AddParameter("meal_des", '');
//    }
   
    //////////New Concept for speed START //////
    
        if (secondPagingFlag==false)
            {
            var str = GetCacheContent_2();
            }

    
    /////////New Concept for speed END ////////
    
    

    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");
    xmlResult = xu.LoadXMLString(xmlResult);

    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("hotelName", '');
    xu.AddParameter("hotelLocation", '');
//    xu.AddParameter("meal_des", '');
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/HotelFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "Hotel_Details/Hotel");



 
    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "xml");
    //document.getElementById("tblResult").innerHTML = strResult;
    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        document.getElementById('Refine_hotellocation').value = "";
//        document.getElementById("Refine_hotelMealplan").value = "";
        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
//        document.getElementById("tprice").className = 'sort-tab info';
    }
    CreatePager(TotalResultCount);
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
}

function hotelResultXML_Filter(sortType, HotelName) {

    $('#waitScreen').css({ 'display': 'block', opacity: 0.7, 'width': $(document).width(), 'height': $(document).height() });
    $('#waitBox').css({ 'display': 'block' });
    var sortType;
    if (sortType != "MIP") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }


    minPrice = document.getElementById("hdnMinPrice").value;
    maxPrice = document.getElementById("hdnMaxPrice").value;
    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", minPrice - 1);
    xu.AddParameter("maxPrice", maxPrice + 1);
    if (trim(HotelName) != "" && trim(HotelName) != null) {
        xu.AddParameter("hotelName", HotelName);
    }
    else {
        xu.AddParameter("hotelName", '');
    }
   
  //////////New Concept for speed START //////
    
        if (secondPagingFlag==false)
            {
            var str = GetCacheContent_2();
            }

    
    /////////New Concept for speed END ////////

    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilter.xslt", "xml");

    xmlResult = xu.LoadXMLString(xmlResult);

    starCount = CheckboxList("chk_starrating");
    xu.AddParameter("starRating", starCount);
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("hotelName", '');
  
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/HotelFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "Hotel_Details/Hotel");
  
    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/CommonResult.xslt", "xml");
    if (TotalResultCount == '0') {
        document.getElementById('Refine_hotelName').value = "";
        
        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnFetchResult()'>View All</a></div>";
    }
    else {
        document.getElementById("tblResult").innerHTML = strResult;
        if (sortType == "MIP") {
//            document.getElementById("tprice").className = 'sort-tab';
//            document.getElementById("tname").className = 'tabs';
//            document.getElementById("trating").className = 'tabs';
        }
        else if (sortType == "HN") {
//        document.getElementById("tprice").className = 'tabs';
//        document.getElementById("tname").className = 'sort-tab';
//        document.getElementById("trating").className = 'tabs';
        }
        else {
//            document.getElementById("tprice").className = 'tabs';
//            document.getElementById("tname").className = 'tabs';
//            document.getElementById("trating").className = 'sort-tab';
        }
    }
    CreatePager(TotalResultCount);
    $('#waitScreen').css({ 'display': 'none' });
    $('#waitBox').css({ 'display': 'none' });
}


function classFilter(r, m, not) {
    m = " " + m + " ";
    var tmp = [];
    for (var i = 0; r[i]; i++) {
        var pass = (" " + r[i].className + " ").indexOf(m) >= 0;
        if (not ^ pass)
            tmp.push(r[i]);
    }
    return tmp;
}


function trim(str) {
    if (!str || typeof str != 'string')
        return null;

    return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
}




//function fillLocation(strResult) {
//    alert(document.getElementById("ddlLocation"));

//    xu = new XML();
//    var xmlLocation;
//    xmlLocation = xu.LoadXMLString(strResult);
//    var ddlValue = document.getElementById('ddlLocation');
//    var countPage = new XML().GetNodeCount(xmlLocation, "Hotel_Details/Hotel/@Location");
//    ddlValue.options[0] = new Option("--select--", "--select--");
//    for (var i = 1; i <= countPage; i++) {
//        var textField = i;
//        var valueField = new XML().SelectNodeValue(xmlLocation, "Hotel_Details/Hotel/@Location[" + i + "]");
//        if (valueField != '' && valueField != null) {
//            ddlValue.options[i] = new Option(valueField, valueField);
//            strOrgLocation += valueField + ','
//        }
//        else
//            strOrgLocation += ',' + ','
//    }

//}

function NoResultDiv() {
    // Hide the Wait DIV.

    var noResultDiv = document.getElementById("noResult");
    noResultDiv.style.display = "block"

}


function CheckboxList(cbControl) {
    var flag = "false";
    var startVal = "";
    var chkBoxList = document.getElementsByName(cbControl);
    for (var i = 0; i < chkBoxList.length; i++) {
        if (chkBoxList[i].checked) {
            if (flag == "false") {
                startVal = chkBoxList[i].value;
                flag = "true";
            }
            else {
                startVal = startVal + "," + chkBoxList[i].value;
            }
        }
    }

    if (startVal == "") {

        startVal = "1,2,3,4,5"; 

    }
    return startVal;
}



function GetCacheContent_2() {

    
    var prefix = ''

    var filename = document.getElementById("cph_main_hdnfile").value + "$" + document.getElementById("cph_main_fileCounter").value;
    xmlHttp  = null;
        xmlHttp = new Ajax().GetXmlHttpObject();

        if (xmlHttp) {
            var webServiceURL;
            
          
           webServiceURL = "AjxHtlResult.aspx?Validate=Paging&FileName=" + filename + "";


//alert(filename);

            xmlHttp.open("GET", webServiceURL, false);
            //xmlHttp.onreadystatechange = fnFetchXML;
           // alert(xmlHttp.readyState);
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlHttp.send(null);
            fnFetchXML();
        
    }
}

function fnFetchXML() {


    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {

        if (xmlHttp.status == 200) {

            var strReply = xmlHttp.responseText

//alert(strReply);

//            strReply = strReply.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
//            strReply = strReply.replace("<string xmlns=\"http://tempuri.org/\">", "");
//            strReply = strReply.replace(/\&lt;/g, "<")
//            strReply = strReply.replace("</string>", "");
//            strReply = strReply.replace(/\&gt;/g, ">")
//            strReply = trim(strReply);

            getresultflag = true;
            xmlHotelResult = xu.LoadXMLString(strReply);
            if (!((strReply == ""))) {
                var strResult;
                xmlOrigHotelResult = xu.LoadXMLString(strReply);
                xmlPagerHotelResult = xu.LoadXMLString(strReply);

                secondPagingFlag = true;
            }


        }

    }
}

function trim(str) {
    if (!str || typeof str != 'string')
        return null;

    return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
}


function CreateHtlNameFilter(txtHotelName) {debugger;

    var HotelName = document.getElementById(txtHotelName).value;
    if (HotelName != null && HotelName != "") {
       // document.getElementById("lblHtlName").innerHTML = HotelName;
      //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        hotelResultXML();
    }
    else
        alert('Please fill Hotel Name');
}
function CreateHtlLocationFilter(txtHotellocation) {debugger;

    var HotelLocation = document.getElementById(txtHotellocation).value;
    if (HotelLocation != null && HotelLocation != "") {
        // document.getElementById("lblHtlName").innerHTML = HotelName;
        //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        hotelResultXML();
    }
    else
        alert('Please fill Hotel Location');
}

function CreateHtlMealFilter(txtHotelMeal) {
    debugger;

    var HotelMeal = document.getElementById(txtHotelMeal).value;
    if (HotelMeal != null && HotelMeal != "") {
        // document.getElementById("lblHtlName").innerHTML = HotelName;
        //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        hotelResultXML();
    }
    else
        alert('Please fill Hotel Location');
}


function ClearHtlNameFilter() {
   // document.getElementById("lblHtlName").innerHTML = "";
    document.getElementById("Refine_hotelName").value = "";
  //  document.getElementById("trHtlNameFliter").style.display = "none";
    hotelResultXML();
}
function ClearHtlLocationFilter() {
    // document.getElementById("lblHtlName").innerHTML = "";
    document.getElementById("Refine_hotellocation").value = "";
    //  document.getElementById("trHtlNameFliter").style.display = "none";
    hotelResultXML();
}

function ClearStarFilter() {
    var chkBoxList = document.getElementsByName("chk_starrating");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked=false;
    }
    hotelResultXML();
}
function SelectStarFilter() {
    var chkBoxList = document.getElementsByName("chk_starrating");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked=true;
    }
    hotelResultXML();
}
function ClearAllFilter() {
   
    var chkBoxList = document.getElementsByName("chk_starrating");
    for (var i = 0; i < chkBoxList.length; i++) {
        chkBoxList[i].checked = false;
    }

    document.getElementById("Refine_hotelName").value = "";

    minPrice = document.getElementById("minAmt").value;
    maxPrice = document.getElementById("maxAmt").value;
    currency = document.getElementById("hdnCurrency").value;
    JquerySlider(parseInt(minPrice), parseInt(maxPrice), currency)

    hotelResultXML();
}

function show(div_price) {debugger;
    document.getElementById(div_price).style.display = "block";
}
function hide(div_price) {
    debugger;
    document.getElementById(div_price).style.display = "none";
}

