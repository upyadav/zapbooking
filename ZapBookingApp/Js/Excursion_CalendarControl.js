var tempDptDay, Type;
var calendarIdentity;
function showCalender(controlId, Type1) {

    Type = Type1;
    showCalendarControl(document.getElementById(controlId));
    calendarIdentity = controlId;

}


function getDateString(day, mon, year) {
    var dateString = "";
    if (mon <= 9 && day <= 9) {
        dateString = "0" + day + " " + GetMonth(mon - 1) + " " + year; //day+"-"+
    }
    else if (mon <= 9 && day > 9) {
        dateString = day + " " + GetMonth(mon - 1) + " " + year; //day+"-"+
    }
    else if (mon > 9 && day <= 9) {
        dateString = "0" + day + " " + GetMonth(mon - 1) + " " + year; //day+"-"+
    }
    else {
        //dateString = day + "/" + mon + "/" + year;
        dateString = day + " " + GetMonth(mon - 1) + " " + year;

    }
    return dateString;
}

function GetMonth(mon) {
    switch (mon) {
        case 0: return "JAN"; break;
        case 1: return "FEB"; break;
        case 2: return "MAR"; break;
        case 3: return "APR"; break;
        case 4: return "MAY"; break;
        case 5: return "JUN"; break;
        case 6: return "JUL"; break;
        case 7: return "AUG"; break;
        case 8: return "SEP"; break;
        case 9: return "OCT"; break;
        case 10: return "NOV"; break;
        case 11: return "DEC";
    }
}
function GetMonthNumber(mon) {
    switch (mon.toUpperCase()) {
        case "JAN": return 1; break;
        case "FEB": return 2; break;
        case "MAR": return 3; break;
        case "APR": return 4; break;
        case "MAY": return 5; break;
        case "JUN": return 6; break;
        case "JUL": return 7; break;
        case "AUG": return 8; break;
        case "SEP": return 9; break;
        case "OCT": return 10; break;
        case "NOV": return 11; break;
        case "DEC": return 12;
    }
}
function getDayName(day) {
    var d = ['(Sun)', '(Mon)', '(Tue)', '(Wed)', '(Thu)', '(Fri)', '(Sat)'];
    return d[day];

}

function positionInfo(object) {

    var p_elm = object;

    this.getElementLeft = getElementLeft;
    function getElementLeft() {
        var x = 0;
        var elm;
        if (typeof (p_elm) == "object") {
            elm = p_elm;
        } else {
            elm = document.getElementById(p_elm);
        }
        while (elm != null) {
            x += elm.offsetLeft;
            elm = elm.offsetParent;
        }
        return parseInt(x, 10);
    }

    this.getElementWidth = getElementWidth;
    function getElementWidth() {
        var elm;
        if (typeof (p_elm) == "object") {
            elm = p_elm;
        } else {
            elm = document.getElementById(p_elm);
        }
        return parseInt(elm.offsetWidth, 10);
    }

    this.getElementRight = getElementRight;
    function getElementRight() {
        return getElementLeft(p_elm) + getElementWidth(p_elm);
    }

    this.getElementTop = getElementTop;
    function getElementTop() {
        var y = 0;
        var elm;
        if (typeof (p_elm) == "object") {
            elm = p_elm;
        } else {
            elm = document.getElementById(p_elm);
        }
        while (elm != null) {
            y += elm.offsetTop;
            elm = elm.offsetParent;
        }
        return parseInt(y, 10);
    }

    this.getElementHeight = getElementHeight;
    function getElementHeight() {
        var elm;
        if (typeof (p_elm) == "object") {
            elm = p_elm;
        }
        else {
            elm = document.getElementById(p_elm);
        }
        return parseInt(elm.offsetHeight, 10);
    }

    this.getElementBottom = getElementBottom;
    function getElementBottom() {
        return getElementTop(p_elm) + getElementHeight(p_elm);
    }
}

function CalendarControl() {

    var calendarId = 'CalendarControl';
    var currentYear = 0;
    var currentMonth = 0;
    var currentDay = 0;

    var selectedYear = 0;
    var selectedMonth = 0;
    var selectedDay = 0;

    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var dateField = null;

    //this.changeMonthInc = changeMonthInc;
    function changeMonthInc(change) {
        currentMonth = new Number(currentMonth)
        currentMonth = new Number(currentMonth) + parseInt(change);
        currentDay = 0;
        if (currentMonth > 12) {
            currentMonth = 1;
            currentYear++;
        }
        else if (currentMonth < 1) {
            currentMonth = 12;
            currentYear--;
        }
    }
    function getProperty(p_property) {
        var p_elm = calendarId;
        var elm = null;

        if (typeof (p_elm) == "object") {
            elm = p_elm;
        } else {
            elm = document.getElementById(p_elm);
        }

        if (elm != null) {
            if (elm.style) {
                elm = elm.style;
                if (elm[p_property]) {
                    return elm[p_property];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        }
    }

    function setElementProperty(p_property, p_value, p_elmId) {
        var p_elm = p_elmId;
        var elm = null;

        if (typeof (p_elm) == "object") {
            elm = p_elm;
        }
        else {
            elm = document.getElementById(p_elm);
        }

        if ((elm != null) && (elm.style != null)) {
            elm = elm.style;
            elm[p_property] = p_value;
        }
    }

    function setProperty(p_property, p_value) {
        setElementProperty(p_property, p_value, calendarId);
    }

    function getDaysInMonth(year, month) {
        return [31, ((!(year % 4) && ((year % 100) || !(year % 400))) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month - 1];
    }

    function getDayOfWeek(year, month, day) {
        var date = new Date(year, month - 1, day)
        return date.getDay();
    }

    this.clearDate = clearDate;
    function clearDate() {
        dateField.value = '';
        hide();
    }

    this.setDate = setDate;
    function setDate(year, month, day) {
        var datestringformate
        var mydate = new Date(year, month - 1, day);
        var dateString = getDateString(mydate.getDate(), mydate.getMonth() + 1, mydate.getFullYear());
        datestringformate = dateString + getDayName(mydate.getDay());
        tempDptDay.value = datestringformate
        if (Type == "D") {
            mydate = new Date(year, month - 1, day + 1);
            dateString = getDateString(mydate.getDate(), mydate.getMonth() + 1, mydate.getFullYear());
            datestringformate = dateString + getDayName(mydate.getDay());
            document.getElementById("Ex1_txt_ChekOut").value = datestringformate;
        }
        if (Type == "S") {
            mydate = new Date(year, month - 1, day + 1);
            dateString = getDateString(mydate.getDate(), mydate.getMonth() + 1, mydate.getFullYear());
            datestringformate = dateString + getDayName(mydate.getDay());
            document.getElementById("Ex1_txt_ChekinDate").value = datestringformate;
        }

        if (Type == "A") {
            mydate = new Date(year, month - 1, day + 1);
            dateString = getDateString(mydate.getDate(), mydate.getMonth() + 1, mydate.getFullYear());
            datestringformate = dateString + "," + getDayName(mydate.getDay());
            document.getElementById("txt_ChkOut_SB").value = datestringformate;
        }

        if (Type == "B") {
            mydate = new Date(year, month - 1, day + 1);
            dateString = getDateString(mydate.getDate(), mydate.getMonth() + 1, mydate.getFullYear());
            datestringformate = dateString + getDayName(mydate.getDay());
            document.getElementById("txtTo").value = datestringformate;
        }
 
        if (document.getElementById("Ex1_txt_ChekOut") != null) {
            if (document.getElementById("Ex1_txt_ChekOut").value != "") {

                var finddays = dateDiff(document.getElementById('Ex1_txt_ChekinDate').value.slice(0, -5), document.getElementById('Ex1_txt_ChekOut').value.slice(0, -5));
               
                if (finddays < 0){
                    alert('check out date should be greater than chek in date.');
                    return false;
                }
                if (finddays == 0) {
                    alert('check out date should be greater than chek in date.Not Equal to check in date.');
                    return false;
                }
                if (finddays > 90) {
                    alert('check out date cannot be exceed 3 month of chek in date.');
                    return false;
                }
                //document.getElementById('cbo_NoOfNights').value = dateDiff(document.getElementById('Ex1_txt_ChekinDate').value.slice(0, -5), document.getElementById('Ex1_txt_ChekOut').value.slice(0, -5));
            }
        }

        hide();
        return;
    }

    this.changeMonth = changeMonth;
    function changeMonth(change) {

        currentMonth += change;
        currentDay = 0;
        if (currentMonth > 12) {
            currentMonth = 1;
            currentYear++;
        }
        else if (currentMonth < 1) {
            currentMonth = parseInt(12 + currentMonth, 10);
            currentYear--;
        }
        calendar = document.getElementById(calendarId);
        calendar.innerHTML = calendarDrawTable();
    }

    this.changeYear = changeYear;
    function changeYear(change) {
        currentYear += change;
        currentDay = 0;
        calendar = document.getElementById(calendarId);
        calendar.innerHTML = calendarDrawTable();
    }

    function getCurrentYear() {
        //    var year = new Date().getFullYear();
        var year = new Date().getFullYear();
        if (year < 1900) year += 1900;
        return year;
    }

    function getCurrentMonth() {
        return new Date().getMonth() + 1;
    }

    function getCurrentDay() {
        return new Date().getDate();
    }

    function calendarDrawTable() {

        var dayOfMonth = 1;
        var validDay = 0;
        var startDayOfWeek;
        var daysInMonth;
        var i;
        var table = "<table  bgcolor='#CBE1F2'>";
        var NoCal = 2;
        try {

            NoCal = document.getElementById("Search").value;
            if (NoCal == "") {
                NoCal = 2
            }
        } catch (ex) {
            NoCal = 2;
        }
        if (NoCal == 2) {
            //table=table+"<tr><td  class='previous' style='border-bottom:1px solid #FCB17F'><a href='javascript:changeCalendarControlMonth(-5);'>&lt;</a><a href='javascript:changeCalendarControlMonth(-5);'>&lt;</a></td>"
            //table=table+"<td style='border-bottom:1px solid #FCB17F; font-weight:bold; color:#ffffff;'>Select Your Travel Date  </td>";
            //table=table+"<td  class='next' align='right' style='border-bottom:1px solid #FCB17F'><a href='javascript:changeCalendarControlMonth(1);'>&gt;</a><a href='javascript:changeCalendarControlMonth(1);'>&gt;</a></td>";  //<a href='javascript:changeCalendarControlYear(1);'>
            //table=table+"</tr>"
            table = table + "<tr><td class='previous' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:changeCalendarControlMonth(-3);'>&lt;</a><a href='javascript:changeCalendarControlMonth(-3);'>&lt;</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:changeCalendarControlMonth(1);'>&gt;</a><a href='javascript:changeCalendarControlMonth(1);'>&gt;</a><td></tr>";

        } else {
            table = table + "<tr><td class='previous'>&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:changeCalendarControlMonth(-1);'>&lt;</a><a href='javascript:changeCalendarControlMonth(-1);'>&lt;</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:changeCalendarControlMonth(1);'>&gt;</a><a href='javascript:changeCalendarControlMonth(1);'>&gt;</a><td></tr>";
        }

        table = table + "<tr>";
        for (i = 1; i <= NoCal; i++) {
            if (i != 1) {
                changeMonthInc(1)
            }
            dayOfMonth = 1;
            validDay = 0;
            //	if(Type=="R")
            //	dayOfMonth= new Number(dayOfMonth)+1;
            startDayOfWeek = getDayOfWeek(currentYear, currentMonth, dayOfMonth);
            daysInMonth = getDaysInMonth(currentYear, currentMonth);
            css_class = null; //CSS class for each day
            table = table + "<td><table cellspacing='0' cellpadding='0' border='0'>";
            table = table + "<tr class='header'>";
            table = table + "  <td colspan='7' class='title' bgcolor='#ffffff' style='color:#000; padding:3px 0 3px 0;'>" + months[currentMonth - 1] + "&nbsp;&nbsp;&nbsp;" + currentYear + "</td>";
            table = table + "</tr>";
            table = table + "<tr bgcolor=lightblue><th bgcolor=red>S</th><th>M</th><th>T</th><th>W</th><th>T</th><th>F</th><th>S</th></tr>";
            //table = table + "<tr bgcolor=lightblue><th bgcolor=red>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>";

            for (var week = 0; week < 6; week++) {
                table = table + "<tr>";
                for (var dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
                    if (week == 0 && startDayOfWeek == dayOfWeek) {
                        validDay = 1;
                    }
                    else if (validDay == 1 && dayOfMonth > daysInMonth) {
                        validDay = 0;
                    }

                    if (validDay) {
                        //if(Type=="R")
                        //selectedDay= new Number(selectedDay)+1;



                        if (dayOfMonth == selectedDay && currentYear == selectedYear && currentMonth == selectedMonth && 1 != 1) {
                            css_class = 'current';
                        }
                        else if (dayOfWeek == 0 || dayOfWeek == 6) {
                            css_class = 'weekend';
                        }
                        else {
                            css_class = 'weekday';
                        }





                        var DMY = new Date();
                        var c;
                        var tempdate = DMY.getDate();
                        var tempMonth = DMY.getMonth();
                        var tempYear = DMY.getFullYear();

                        if (Type == "R") {

                          
                            temp_arry = new Array();
                            if (document.getElementById("Ex1_txt_ChekinDate").value == "") {
                                alert("Please select check in date:");
                                return;
                            }
                            else {
                                //temp_arry = document.getElementById("txt_ChekinDate").value.split("/");
                                temp_arry = document.getElementById("Ex1_txt_ChekinDate").value.split(" ");
                                tempdate = new Number(temp_arry[0]) + 1;
                                tempMonth = new Number(temp_arry[1] - 1);
                                tempYear = new Number(temp_arry[2]);

                            }

                        }



                        var perDate = new Date(tempYear, tempMonth, tempdate);
                        var CurDate = new Date(currentYear, currentMonth - 1, dayOfMonth);
                        //					  alert("perdate:" +perDate);
                        //					  alert("CurDate:" +CurDate);
                        //var LastDate=new Date(DMY.getFullYear(),DMY.getMonth()+11,DMY.getDate());
                        //if(CurDate<perDate || CurDate>LastDate)



                        if (CurDate < perDate) {
                            table = table + "<td><a class='" + css_class + "'><strike><font color='#c1c1c1'>" + dayOfMonth + "</font></strike></a></td>";
                        }
                        else {
                            table = table + "<td><a class='" + css_class + "' href=\"javascript:setCalendarControlDate(" + currentYear + "," + currentMonth + "," + dayOfMonth + ")\">" + dayOfMonth + "</a></td>";
                        }

                        dayOfMonth++;
                    }
                    else {
                        table = table + "<td class='empty'>&nbsp;</td>";
                    }
                }
                table = table + "</tr>";
            }
            table = table + "</table></td>";
        }
      
        table = table + "</tr>"
        table = table + "<tr class='header'><th colspan='" + NoCal + "' style='padding: 3px;'><a href='javascript:hideCalendarControl();'><font color=black><b>Close</b></font></a></th></tr>"
        table = table + "</table>";
        return table
    }



    this.show = show;
    function show(temDD) {
        can_hide = 0;
        if (dateField == temDD) {
            return;
        }
        else {
            dateField = temDD;
        }

        if (dateField) {
            try {

               
                if (Type == "R") {
                    //	    selectedMonth = parseInt(document.getElementById(temMMYear.id).value.substring(0,2),10);
                    //	    selectedYear = parseInt(parseInt(document.getElementById(temMMYear.id).value.substring(2,6),10),10);
                    //	    selectedDay = parseInt(document.getElementById(temDD.id).value,10);
                    temp_arry = new Array();
                    if (document.getElementById("Ex1_txt_ChekinDate").value == "") {
                        alert("Please select check in date:");
                        return;
                    }
                    else {
                       

                        temp_arry = document.getElementById("Ex1_txt_ChekOut").value.slice(0, -5).split(" ");
                        //temp_arry = document.getElementById("txt_ChekOut").value.slice().slice(0, -4).split("/"); 
                        selectedMonth = new Number(temp_arry[1]);
                        selectedYear = new Number(temp_arry[2]);
                        selectedDay = new Number(temp_arry[0]);
                    }
                }
                ////////////////////////////////////
             
                var controlId = Type == "R" ? "Ex1_txt_ChekOut" : "Ex1_txt_ChekinDate";
                if (document.getElementById(controlId).value != "") {
                    currarry = document.getElementById(controlId).value.slice(0, -5).split(" ");
                    GetMonthNumber
                    selectedMonth = GetMonthNumber(currarry[1]);
                    //selectedMonth=new Number (currarry[1]);
                    selectedYear = new Number(currarry[2]);


                }
               
                ///////////////////////////////////   





            } catch (e) { }
        }
        if (!(selectedYear && selectedMonth && selectedDay)) {
            selectedMonth = getCurrentMonth();
            selectedDay = getCurrentDay();
            selectedYear = getCurrentYear();
        }

        currentMonth = selectedMonth;
        currentDay = selectedDay;
        currentYear = selectedYear;
        if (document.getElementById) {
            calendar = document.getElementById(calendarId);

            calendar.innerHTML = calendarDrawTable(currentYear, currentMonth);
           
            setProperty('display', 'block');

            var fieldPos = new positionInfo(tempDptDay);
            var calendarPos = new positionInfo(calendarId);

            var x = fieldPos.getElementLeft();
            var y = fieldPos.getElementBottom();
            setProperty('left', x + "px");
            setProperty('top', y + "px");

            if (document.all) {
                setElementProperty('display', 'block', 'CalendarControlIFrame');
                setElementProperty('left', x + "px", 'CalendarControlIFrame');
                setElementProperty('top', y + "px", 'CalendarControlIFrame');
                setElementProperty('width', calendarPos.getElementWidth() + "px", 'CalendarControlIFrame');
                setElementProperty('height', calendarPos.getElementHeight() + "px", 'CalendarControlIFrame');
            }
        }
    }

    this.hide = hide;
    function hide() {

        setProperty('display', 'none');
        setElementProperty('display', 'none', 'CalendarControlIFrame');
        dateField = null;

    }

    this.visible = visible;
    function visible() {

        return dateField
    }

    this.can_hide = can_hide;
    var can_hide = 0;
}



var calendarControl = new CalendarControl();
function showCalendarControl(temdptDD) {

   
    tempDptDay = temdptDD;
    calendarControl.show(tempDptDay);

}



function clearCalendarControl() {
    calendarControl.clearDate();
}

function hideCalendarControl() {
    if (calendarControl.visible()) {
        calendarControl.hide();
    }
}

function setCalendarControlDate(year, month, day) {

    calendarControl.setDate(year, month, day);
    
}

function changeCalendarControlYear(change) {
    calendarControl.changeYear(change);
}

function changeCalendarControlMonth(change) {
    calendarControl.changeMonth(change);
}

document.write("<iframe id='CalendarControlIFrame'  frameBorder='0' scrolling='no'></iframe>");
document.write("<div id='CalendarControl' id='cal' style='z-index:4;'></div>");


function getDaysInMonth(strMonth, strYear) {
    var strDays
    switch (parseInt(strMonth, 10)) {
        case 1: strDays = 31; break;
        case 3: strDays = 31; break;
        case 5: strDays = 31; break;
        case 7: strDays = 31; break;
        case 8: strDays = 31; break;
        case 10: strDays = 31; break;
        case 12: strDays = 31; break;
        case 4: strDays = 30; break;
        case 6: strDays = 30; break;
        case 9: strDays = 30; break;
        case 11: strDays = 30; break;
        case 2:
            if ((parseInt(strYear, 10) % 4 == 0 && parseInt(strYear, 10) % 100 != 0) || (parseInt(strYear, 10) % 400 == 0)) {
                strDays = 29;
            }
            else {
                strDays = 28;
            }

    }
    return strDays;
}
function dateDiff(strDate1, strDate2) {
    strDate1 = strDate1.split(" ");
    starttime = new Date(strDate1[2], GetMonthNumber(strDate1[1]) - 1, strDate1[0]);
    starttime = new Date(starttime.valueOf());

    //End date split to UK date format 
    strDate2 = strDate2.split(" ");
    endtime = new Date(strDate2[2], GetMonthNumber(strDate2[1]) - 1, strDate2[0]);
    endtime = new Date(endtime.valueOf());

    var diff = (endtime - starttime) / (1000 * 60 * 60 * 24);
    return diff;
}




function on_date_change1() {

    var dt_checkin = "";
    var dt_checkOut = "";
    var no_of_nights = 0;


    if (document.forms[0].txtCheckInDate.value != "") {
        no_of_nights = document.forms[0].cbo_noofnights.options[document.forms[0].cbo_noofnights.selectedIndex].value;
        dt_checkin = document.forms[0].txtCheckInDate.value;




        dt = new String(dt_checkin);
        dt_arry = new Array(2);
        //dt_arry=dt.split("-");
        dt_arry = dt.split("/");

        /*if(dt_arry.length != 3)
        {
        alert('Please enter date in dd-mm-yyyy format' );
        document.forms[0].txt_ChekinDate.value="";
        document.Search_Result.txt_ChekinDate.focus();
        return false;
        }*/

        var tmp_dt;
        var tmp_mnth;
        var tmp_yr;



        if (dt.charAt(3) == '0') {
            //alert(dt_arry[1])
            dt_arry[1] = dt.charAt(4);
            //alert(dt_arry[1])
        }
        if (dt.charAt(0) == 0) {
            dt_arry[0] = dt.charAt(1);
            //alert(dt_arry[2])
        }

        //validating checkin date should be in proper format-----
        //var test_Date=dt_arry[1]+'/'+dt_arry[2]+'/'+dt_arry[0];
        var test_Date = dt_arry[0] + '/' + dt_arry[1] + '/' + dt_arry[2];


        //------------------------------------------------------------

        tmp_dt = parseInt(dt_arry[0]) + parseInt(no_of_nights);
        tmp_dt = tmp_dt;
        tmp_mnth = dt_arry[1];
        tmp_mnth = tmp_mnth;
        dt_tmp = new Date(dt_arry[2] + '/' + tmp_mnth + '/' + tmp_dt);


        //Break Date Down
        tmp_dt = dt_tmp.getDate().toString()
        tmp_mnth = dt_tmp.getMonth().toString()
        var tmp_Year = dt_tmp.getFullYear().toString()


        //Add a zero to day 
        if (tmp_dt.valueOf() < 10) {
            tmp_dt = "0" + tmp_dt
        }
        var mth = tmp_mnth;
        if (tmp_mnth.valueOf() < 9) {
            tmp_mnth = "0" + tmp_mnth
        }

        var monthName
        monthName = new Array();
        monthName[0] = "Jan";
        monthName[1] = "Feb";
        monthName[2] = "Mar";
        monthName[3] = "Apr";
        monthName[4] = "May";
        monthName[5] = "Jun";
        monthName[6] = "Jul";
        monthName[7] = "Aug";
        monthName[8] = "Sep";
        monthName[9] = "Oct";
        monthName[10] = "Nov";
        monthName[11] = "Dec";

        //dt_checkOut= dt_arry[0] + ' - ' + monthName[eval(tmp_mnth -1)] + ' - ' + dt_tmp.getDate();
        //dt_checkOut= tmp_dt+ '-' + monthName[mth] + '-' + tmp_Year;

        document.forms[0].txtCheckOutDate.value = "";
        /////////document.forms[0].txt_ChekOut_yyyyMMdd.value="";
        //Add a zero to month
        var tmp_mth = 0;

        //if (tmp_mnth.valueOf() < 10)
        //{
        //	tmp_mth=eval(Number(tmp_mnth) + 1);
        //	tmp_mth = "0" +  tmp_mth;
        //}
        //else
        //{
        tmp_mth = eval(Number(tmp_mnth));
        if (tmp_mth.valueOf() < 10) {
            tmp_mth = eval(Number(tmp_mth) + 1);
            if (tmp_mth.valueOf() < 10) {
                tmp_mth = "0" + tmp_mth;
            }
        }
        else {
            tmp_mth = eval(Number(tmp_mth) + 1);

        }
        dt_checkOut = tmp_dt + '/' + tmp_mth + '/' + tmp_Year;

        document.forms[0].txtCheckOutDate.value = dt_checkOut;
        ////////document.forms[0].txt_ChekOut_yyyyMMdd.value=tmp_Year + "-" + tmp_mth + "-" + tmp_dt;

        //alert(document.forms[0].txt_ChekOut_yyyyMMdd.value)
        //validating the checkin date should not be less than today date--------------------------------

        var testDate = new Date(dt_arry[1] + '/' + dt_arry[0] + '/' + dt_arry[2]);

        var month;
        var year;
        var day;

        month = testDate.getMonth();
        year = testDate.getYear();
        day = testDate.getDay();

    }

}
function on_date_change() {

    var dt_checkin = "";
    var dt_checkOut = "";
    var no_of_nights = 0;


    if (document.forms[0].txt_ChekinDate.value != "") {
        no_of_nights = document.forms[0].cbo_NoOfNights.options[document.forms[0].cbo_NoOfNights.selectedIndex].value;
        dt_checkin = document.forms[0].txt_ChekinDate.value;


        dt_checkin = dt_checkin.slice(0, -5);

        dt = new String(dt_checkin);
        dt_arry = new Array(2);

        dt_arry = dt.split(" ");



        var tmp_dt;
        var tmp_mnth;
        var tmp_yr;



        if (dt.charAt(3) == '0') {

            dt_arry[1] = dt.charAt(4);

        }
        if (dt.charAt(0) == 0) {
            dt_arry[0] = dt.charAt(1);

        }


        var test_Date = dt_arry[0] + '/' + dt_arry[1] + '/' + dt_arry[2];


        //------------------------------------------------------------

        tmp_dt = parseInt(dt_arry[0]) + parseInt(no_of_nights);
        tmp_dt = tmp_dt;
        tmp_mnth = GetMonthNumber(dt_arry[1]);
        tmp_mnth = tmp_mnth;
        dt_tmp = new Date(dt_arry[2] + '/' + tmp_mnth + '/' + tmp_dt);


        //Break Date Down
        tmp_dt = dt_tmp.getDate().toString()
        tmp_mnth = dt_tmp.getMonth().toString()
        var tmp_Year = dt_tmp.getFullYear().toString()
        tmp_mth = tmp_mnth

        //Add a zero to day 



        //        document.forms[0].txt_ChekOut.value = "";
        //        document.forms[0].txt_ChekOut_yyyyMMdd.value = "";
        //        var tmp_mth = 0;


        //        tmp_mth = eval(Number(tmp_mnth));
        //        if (tmp_mth.valueOf() < 10) {
        //            tmp_mth = eval(Number(tmp_mth) + 1);
        //            if (tmp_mth.valueOf() < 10) {
        //                tmp_mth = "0" + tmp_mth;
        //            }
        //        }
        //        else {
        //            tmp_mth = eval(Number(tmp_mth) + 1);

        //        }
        dt_checkOut = tmp_dt + '/' + tmp_mth + '/' + tmp_Year;


        dtDisplay = new Date(tmp_Year, tmp_mth, tmp_dt);

        var day = eval(Number(dtDisplay.getDate()));
        if (day.valueOf() < 10) {
            day = "0" + day;
        }

        dt_checkOut = day + ' ' + GetMonth(dtDisplay.getMonth()) + ' ' + dtDisplay.getFullYear() + getDayName(dtDisplay.getDay()); ;

        document.forms[0].txt_ChekOut.value = dt_checkOut;
        // document.forms[0].txt_ChekOut_yyyyMMdd.value = tmp_Year + "-" + tmp_mth + "-" + tmp_dt;



        var testDate = new Date(dt_arry[1] + '/' + dt_arry[0] + '/' + dt_arry[2]);

        var month;
        var year;
        var day;

        month = testDate.getMonth();
        year = testDate.getYear();
        day = testDate.getDay();
    }
}






