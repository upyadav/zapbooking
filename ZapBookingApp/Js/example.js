$(document).ready(function() {
    $('#starRating').ratings(7).bind('ratingchanged', function(event, data) {
        $('#starRatingCount').text(data.rating);
        StarRating(data.rating);
    });

});