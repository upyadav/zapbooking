﻿
// JScript File
var TotalChd1 = 3
var TotalChd2 = 3
var TotalChd3 = 3
//var bFirstResult=true
var xmlHotelResult;
var xmlOrigHotelResult;
var xmlPagerHotelResult;
var resultXml = "";
var _splitRT
var totalNoStar
var HotelStar
var sortRb = 'PR'
var decimalPref = "false"
var xu = new XML();
var dom = new DOM();
var currFactor = 1.0;
var setDisplayCurrency = false;
var TotalResultCount;
var pages;
var PagingCounter1 = 29;
var strOrgLocation = ',';
var scripts = new Array();
var scripts1 = new Array();
var Allmarker = new Array();
var cm_mapHTMLS = new Array();
var globalStartPos = 0;
var xmlHttp = null;
var minPrice = 10;
var maxPrice = 3000;
var starCount = 1;
var priceFilter = false;
var starFilter = false;
var xmlpriceSightResult;
var xmlstarHotelResult;
var display = "List";


function fnGetExcursionResult() {
    var prefix = ''
    debugger;
    xmlHttp = new Ajax().GetXmlHttpObject();
    
    if (xmlHttp) {
        var webServiceURL;
        webServiceURL = "ExcursionAjxResult.aspx?Validate=ExcursionResult"
        xmlHttp.open("POST", webServiceURL, true);
        xmlHttp.onreadystatechange = fnFetchExcursionResult;
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
    }
}


function fnFetchExcursionResult() {
    debugger;
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
        debugger;
        if (xmlHttp.status == 200) {
            debugger;
            ToggleComponents();
            debugger;
            var strReply = xmlHttp.responseText
            xmlHotelResult = xu.LoadXMLString(strReply);

            if (!((strReply == "") || (xmlHotelResult.firstChild.nodeName == "ErrorMessages"))) {
                var strResult;
                TotalResultCount = new XML().GetNodeCount(xmlHotelResult, "SS_Details/SightSeeing");
                xmlOrigHotelResult = xu.LoadXMLString(strReply);
                xmlPagerHotelResult = xu.LoadXMLString(strReply);

                xu.AddParameter("startIndex", "1");
                xu.AddParameter("endIndex", (PagingCounter1 + 1).toString()); //put here 51
                strResult = xu.XSLTransform(xmlHotelResult, "../XSLT/SightseenResult.xslt", "xml");
                document.getElementById("tblResult").innerHTML = strResult;
                document.getElementById("lblTotal").innerHTML = TotalResultCount;
                CreatePager1(TotalResultCount);
                minPrice = new XML().SelectNodeValue(xmlHotelResult, "/Excursion_Response/Master/@GMin_Price");
                maxPrice = new XML().SelectNodeValue(xmlHotelResult, "/Excursion_Response/Master/@GMax_Price");
                initSlider(true, minPrice, maxPrice);
                SightResultXML();

            }
            else {
                NoResultDiv();
                minPrice = 0;
                maxPrice = 0;
                initSlider(false, minPrice, maxPrice);
                document.getElementById("cntrlRefine").style.display = 'none';
            }

        }
    }
}



function fnGetResult(currencyname) {
    debugger;
    var prefix = ''
    var currencyname = currencyname;
    xmlHttp = new Ajax().GetXmlHttpObject();

    if (xmlHttp) {
        var webServiceURL;
        webServiceURL = "ExcursionAjxResult.aspx?Validate=currrency&currencyname=" + currencyname + ""
        xmlHttp.open("POST", webServiceURL, true);
        xmlHttp.onreadystatechange = fnFetchExcursionResult;
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.send(null);
    }
}




function SightResultXML() {
    debugger;
  
    var sortType = "SName";
    if (sortType == "SName") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }

    minPrice = dom.GetElementInnerText("Price-LeftText");
    maxPrice = dom.GetElementInnerText("Price-RightText");
    xu.AddParameter("minPrice", parseInt(minPrice) - 1);
    xu.AddParameter("maxPrice", parseInt(maxPrice) + 1);

    var SName = document.getElementById("cph_main_Refine_SName").value;
    if (trim(SName) != "" && trim(SName) != null) {
        xu.AddParameter("SName",SName);
    }
    else {
        xu.AddParameter("SName", '');
    }


    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/ExcursionFilter.xslt", "xml");
    xmlResult = xu.LoadXMLString(xmlResult);


    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    xu.AddParameter("SName", '');
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/ExcursionFilterSorting.xslt", "xml");


    priceFilter = true;
    xmlpriceSightResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceSightResult, "SS_Details/SightSeeing");

    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter1 + 1).toString()); //put here 51
    strResult = xu.XSLTransform(xmlpriceSightResult, "../XSLT/SightseenResult.xslt", "xml");
    document.getElementById("tblResult").innerHTML = strResult;
    document.getElementById("lblTotal").innerHTML = TotalResultCount;
    CreatePager1(TotalResultCount);
    localtblLength = document.getElementById("tblPagingbottom").rows.length
    if (localtblLength == 0) {
        for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
            document.getElementById('tblPagingbottom').deleteRow(0)
        }
        var rowIndxbottom = document.getElementById("tblPagingbottom").rowIndex;
        var newRowbottom = dom.InsertTableRow("tblPagingbottom", 0);
        newCellbottom = dom.InsertTableCell(newRowbottom);
        newCellbottom.innerHTML = "<a style='color:blue;cursor: pointer' onclick='javascript:fnGetExcursionResult()'>View All</a>";
    }

}

//function CreateSlider(minPrice, maxPrice) {

//    var a = 10;
//    alert(a);
//    var b = 1000;
//    alert(b);
//    document.getElementById('Refine_minValue').value = minPrice;
//    document.getElementById('Refine_maxValue').value = maxPrice;
//}
function resultSortBySName(SName) {
    debugger;
    
    if (SName != "" && SName != null) {
        SightResultFilter("SName", SName);
        //alert('ok');
    }
    else {
        alert('Please fill Sight Name');
    }
}
function SightResultFilter(sortType, SName) {

    debugger;
    var sortDType;
    if (sortType != "Min_Price") {
        sortDType = "text";
    }
    else {
        sortDType = "number";
    }


    minPrice = dom.GetElementInnerText("Price-LeftText");
    maxPrice = dom.GetElementInnerText("Price-RightText");
    xu.AddParameter("minPrice", minPrice - 1);
    xu.AddParameter("maxPrice", maxPrice + 1);
    if (SName != "" && SName != null) {
        xu.AddParameter("SName", SName);
    }
    else {
        xu.AddParameter("SName", '');
    }
    var xmlResult = xu.XSLTransform(xmlHotelResult, "../XSLT/ExcursionFilter.xslt", "xml");
    xmlResult = xu.LoadXMLString(xmlResult);
    xu.AddParameter("minPrice", '');
    xu.AddParameter("maxPrice", '');
    //xu.AddParameter("SName", '');
    xu.AddParameter("sortType", sortType);
    xu.AddParameter("sortDataType", sortDType);
    var xmlResut = xu.XSLTransform(xmlResult, "../XSLT/ExcursionFilterSorting.xslt", "xml");
    priceFilter = true;
    xmlpriceHotelResult = xu.LoadXMLString(xmlResut);

    TotalResultCount = new XML().GetNodeCount(xmlpriceHotelResult, "SS_Details/SightSeeing");
    xu.AddParameter("startIndex", "1");
    xu.AddParameter("endIndex", (PagingCounter1 + 1).toString()); //put here 51
    
    strResult = xu.XSLTransform(xmlpriceHotelResult, "../XSLT/SightseenResult.xslt", "xml");
    document.getElementById("tblResult").innerHTML = strResult;
    document.getElementById("lblTotal").innerHTML = TotalResultCount;
    CreatePager1(TotalResultCount);

    if (TotalResultCount == '0') {
        document.getElementById("tblResult").innerHTML = "<div class='blackboldtxt11'>No Result Found " + "<a style='color:blue;cursor: pointer' onclick='javascript:fnGetExcursionResult()'>View All</a></div>";
    }
    else {

        document.getElementById("tblResult").innerHTML = strResult;
        document.getElementById("lblTotal").innerHTML = TotalResultCount;
        localtblLength = document.getElementById("tblPaging").rows.length
        for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
            document.getElementById('tblPaging').deleteRow(0)
        }
        localtblLength = document.getElementById("tblPagingbottom").rows.length
        for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
            document.getElementById('tblPagingbottom').deleteRow(0)
        }

        var rowIndx = document.getElementById("tblPaging").rowIndex;
        newRow = dom.InsertTableRow("tblPaging", rowIndx + 1);

        var rowIndxbottom = document.getElementById("tblPagingbottom").rowIndex;
        var newRowbottom = dom.InsertTableRow("tblPagingbottom", 0);
        newCellbottom = dom.InsertTableCell(newRowbottom);
        document.getElementById("cph_main_Refine_SName").value = "";
        newCellbottom.innerHTML = "<a style='color:blue;cursor: pointer' onclick='javascript:fnGetExcursionResult()'>View All</a>";
    }

}

//function resultSort(sortType) {

//    var sortDType;
//    if (sortType == "Min_Price") {
//        sortDType = "number";
//    }
//    else {
//        sortDType = "text";
//    }

//    xu.AddParameter("starRating", '');
//    xu.AddParameter("minPrice", '');
//    xu.AddParameter("maxPrice", '');
//    xu.AddParameter("sortType", sortType);
//    xu.AddParameter("sortDataType", sortDType);
//    var xmlResut = xu.XSLTransform(xmlHotelResult, "../XSLT/HotelFilterSorting.xslt", "xml");
//    priceFilter = true;
//    xmlpriceSightResult = xu.LoadXMLString(xmlResut);

//    TotalResultCount = new XML().GetNodeCount(xmlpriceSightResult, "Hotel_Details/Hotel");

//    xu.AddParameter("startIndex", "1");
//    xu.AddParameter("endIndex", (PagingCounter1 + 1).toString()); //put here 51
//    strResult = xu.XSLTransform(xmlpriceSightResult, "../XSLT/CommonResult.xslt", "xml");
//    document.getElementById("tblResult").innerHTML = strResult;
//    CreatePager1(TotalResultCount);

//}





   function CreatePager1(totResults) {
       debugger;
    
    var localtblLength = document.getElementById("tblPaging").rows.length
    for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
        document.getElementById('tblPaging').deleteRow(0)
    }
    localtblLength = document.getElementById("tblPagingbottom").rows.length
    for (intCtr = 0; intCtr <= localtblLength - 1; intCtr++) {
        document.getElementById('tblPagingbottom').deleteRow(0)
    }

    var rowIndx = document.getElementById("tblPaging").rowIndex;
    newRow = dom.InsertTableRow("tblPaging", rowIndx + 1);

    var rowIndxbottom = document.getElementById("tblPagingbottom").rowIndex;
    var newRowbottom = dom.InsertTableRow("tblPagingbottom", rowIndxbottom + 1);
    var newCellbottom;

    var counPage = parseFloat(totResults / PagingCounter1).toString().split('.')
    if (counPage.length == 2) {
        pages = parseInt(counPage[0]) + 1;
    }
    else {
        pages = counPage[0];
    }
    var FirstPage = 1;
    var NextPage;
 
    newCell = dom.InsertTableCell(newRow);
    newCell.className = "blackboldtxt11";
    newCell.width = "100%"
    newCell.align = "left";
//    newCell.innerHTML = "Total Results: " + totResults;
    for (var _iPage = 1; _iPage <= pages; _iPage++) {
        newCell = dom.InsertTableCell(newRow);
        newCellbottom = dom.InsertTableCell(newRowbottom);

        NextPage = parseInt(FirstPage) + PagingCounter1;
        if (_iPage != pages) {
            if (_iPage == 1) {
                newCell.innerHTML = "<a class='dark-themecurrent' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' style='color:black; cursor:pointer; font-weight:bold;text-decoration:none;'> " + _iPage.toString() + "</span></a>&nbsp;|"
                newCellbottom.innerHTML = "<a class='dark-themecurrent' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:black; cursor:pointer; font-weight:bold;text-decoration:none;'> " + _iPage.toString() + "</span></a>&nbsp;|"
            }
            else {
                newCell.innerHTML = "<a class='dark-theme' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' style='color:blue;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;|"
                newCellbottom.innerHTML = "<a class='dark-theme' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + NextPage + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:blue;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;|"
            }
        }
        else {
            var totalres = totResults + 1;
            if (_iPage == 1) {
                newCell.innerHTML = "<a class='dark-theme' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + totalres + "," + _iPage + ")' style='text-decoration:none;'><span id='spnPageAll" + _iPage.toString() + "' style='color:black; cursor:pointer; font-weight:bold;text-decoration:none;'> </span></a>&nbsp;"
                newCellbottom.innerHTML = "<a class='dark-theme' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + totalres + "," + _iPage + ")' style='text-decoration:none;'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:black; cursor:pointer; font-weight:bold;text-decoration:none;'> </span></a>&nbsp;"
            }
            else {
                newCell.innerHTML = "<a class='dark-theme' id='pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + totalres + "," + _iPage + ")'><span id='spnPageAll" + _iPage.toString() + "' style='color:blue;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;"
                newCellbottom.innerHTML = "<a class='dark-theme' href='#pg" + _iPage.toString() + "' onclick='javascript:ShowPaging1(" + FirstPage + "," + totalres + "," + _iPage + ")'><span id='spnPageAllbottom" + _iPage.toString() + "' style='color:blue;cursor: pointer'> " + _iPage.toString() + "</span></a>&nbsp;"
            }
        }
        FirstPage = parseInt(FirstPage) + PagingCounter1;
    }
}

function ShowPaging1(startpos, endpos, currposCount) {
    // window.setTimeout('ShowPaging1Main' + '(\'' + startpos + '\',\'' + endpos + '\',\'' + currposCount + '\')', 1);
    // window.setTimeout(''+funName+''+'(\''+Parm+'\')',1);
    //ToggleLayer(true);
    ShowPaging1Main(startpos, endpos, currposCount);
}

function ShowPaging1Main(start, end, posCount) {
    
    for (var _iPage = 1; _iPage <= pages; _iPage++) {
        if (document.getElementById("spnPageAll" + _iPage)) {
            if (_iPage == posCount) {
                document.getElementById("spnPageAll" + _iPage).style.color = 'black';
                document.getElementById("spnPageAll" + _iPage).style.fontWeight = "bold"
//                document.getElementById("spnPageAllbottom" + _iPage).style.color = 'black';
//                document.getElementById("spnPageAllbottom" + _iPage).style.fontWeight = "bold"
            }
            else {
                document.getElementById("spnPageAll" + _iPage).style.color = 'blue';
                document.getElementById("spnPageAll" + _iPage).style.fontWeight = "normal"
//                document.getElementById("spnPageAllbottom" + _iPage).style.color = 'blue';
//                document.getElementById("spnPageAllbottom" + _iPage).style.fontWeight = "normal"
            }
        }
    }
    xu = new XML();
    xu.AddParameter("startIndex", start);
    xu.AddParameter("endIndex", end);
    var xmlResut = xu.XSLTransform(xmlHotelResult, "../XSLT/ExcursionFilterSorting.xslt", "xml");
    priceFilter = true;
    xmlpriceSightResult = xu.LoadXMLString(xmlResut);
    xu.AddParameter("startIndex", start);
    xu.AddParameter("endIndex", end);
    strResult = xu.XSLTransform(xmlpriceSightResult, "../XSLT/SightseenResult.xslt", "xml");
    //if (priceFilter == true) {
    //strResult = xu.XSLTransform(xmlHotelResult, "../XSLT/ExcursionResponse.xslt", "html");

//    }
//    else if (starFilter == true) {
    //    strResult = xu.XSLTransform(xmlstarHotelResult, "../XSLT/ExcursionResponse.xslt", "html");

//    }
//    else {
    //        strResult = xu.XSLTransform(xmlPagerHotelResult, "../XSLT/ExcursionResponse.xslt", "html");
//    }
    //xmlHotelResult = xu.LoadXMLString(strResult);

    xmlHotelResult = xmlPagerHotelResult;
    document.getElementById("tblResult").innerHTML = strResult;








    //strResult = FilterResults();
    //tblLength = document.getElementById("tblResults").rows.length
    //    for (intCtr = 0; intCtr <= tblLength - 1; intCtr++) {
    //        document.getElementById("tblResults").deleteRow(0)
    //    }
    //    newrow = dom.InsertTableRow("tblResults", 0)
    //    newCell = dom.InsertTableCell(newrow, 0);
    //    strResult = strResult.replace(/#####/g, '');
    //    newCell.innerHTML = strResult
    //    var count = new XML().GetNodeCount(xmlHotelResult, "Hotel_Details/Hotel");
    //  dom.SetElementInnerText("spnpageResult", start);
    // var maxpag = end - 1;
    // dom.SetElementInnerText("spntotalpager", maxpag.toString());
    // fnCalcPrice('ALL', 0, '')
    // ShowResult('', parseInt(start - 1));
    // ToggleLayer(false);
}

function ToggleComponents() {debugger;
    // Hide the Wait DIV.
    var waitDiv = document.getElementById("divWait");
    if (waitDiv) {
        waitDiv.style.display = 'None';
//        waitDiv.style.visibility = "hidden"
//        waitDiv.style.position = "absolute"
    }

    // Show the Results DIV.
    var resultsDiv = document.getElementById("wrapper");
    resultsDiv.style.display = ""

}
//-------------Anand-------------
function initSlider(bCreateFilterSlider, minPrice, maxPrice) {
    debugger;

    if (decimalPref == "false") {
        createSlider('Price', minPrice, parseFloat(maxPrice) + 1, 0)
    }
    else {
        createSlider('Price', parseInt(minPrice), parseInt(maxPrice) + 1, 1)
    }
}
function createSlider(divID, minVal, maxVal, decPlaces) {
    debugger;
    //var decSeparator = document.getElementById("ctl00_hdnDecSeparator").value;
    var decSeparator = ".";
    var DoubleTrackBar = new cDoubleTrackBar(divID, divID + '-Tracker',
    {
        Min: parseInt(minVal),
        Max: parseInt(maxVal),
        FingerOffset: 1,
        MinSpace: 0,
        RoundTo: 1,
        Margins: 10,
        Digits: decPlaces,
        DecimalSeperator: decSeparator,
        OnUpdate: function () {

            
            var sMinPriceInnerHtml, sMaxPriceInnerHtml;

            // setting the left and right dynamo texts
            dom.$(divID + '-LeftText').innerHTML = "";
            dom.$(divID + '-RightText').innerHTML = "";
            dom.$(divID + '-LeftText').innerHTML = this.MinPos;
            dom.$(divID + '-RightText').innerHTML = this.MaxPos;

            // setting the min and max scale values to reflect the dynamo texts - as per ORDERS !
            if ((dom.$('Min' + divID) != null) && (dom.$('Max' + divID) != null)) {
                dom.$('Min' + divID).innerHTML = "";
                dom.$('Max' + divID).innerHTML = "";

                // sMinPriceInnerHtml = "<span class='LeftSpan'>" + addCommas(this.MinPos)
                // sMaxPriceInnerHtml = "<span class='RightSpan'>" + addCommas(this.MaxPos)
                sMinPriceInnerHtml = "<span class='LeftSpan'>" + this.MinPos
                sMaxPriceInnerHtml = "<span class='RightSpan'>" + this.MaxPos


                if (this.Digits == 1) {
                    sMinPriceInnerHtml += this.DecimalSeperator + "00</span>";
                    sMaxPriceInnerHtml += this.DecimalSeperator + "00</span>";
                }
                else {
                    sMinPriceInnerHtml += "</span>";
                    sMaxPriceInnerHtml += "</span>";
                }
                dom.$('Min' + divID).innerHTML = sMinPriceInnerHtml;
                dom.$('Max' + divID).innerHTML = sMaxPriceInnerHtml;
            }
            this.Tracker.style.backgroundPosition = -this.TrackerLeft + 'px center';
            if (this.fix)
                for (var i in this.fix)
                    this.fix[i].style.left = (this.TrackerRight - this.TrackerLeft) + 'px';
        }
		  ,
        OnComplete: setDivToggleFilterSearch
    });
    DoubleTrackBar.AutoHairline(2);
    DoubleTrackBar.fix = classFilter(DoubleTrackBar.Tracker.getElementsByTagName('*'), 'flr');
}
function classFilter(r, m, not) {
    m = " " + m + " ";
    var tmp = [];
    for (var i = 0; r[i]; i++) {
        var pass = (" " + r[i].className + " ").indexOf(m) >= 0;
        if (not ^ pass)
            tmp.push(r[i]);
    }
    return tmp;
}


function trim(str) {
    if (!str || typeof str != 'string')
        return null;

    return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
}
function setDivToggleFilterSearch() {
    debugger;
    window.setTimeout(SightResultXML, 1);
    //ToggleLayer(true);
}
function NoResultDiv() {
    // Hide the Wait DIV.

    var noResultDiv = document.getElementById("noResult");
    noResultDiv.style.display = ""

}

function CreateHtlNameFilter(txtSightName) {
    debugger;

    var HotelName = document.getElementById(txtSightName).value;
    if (HotelName != null && HotelName != "") {
        // document.getElementById("lblHtlName").innerHTML = HotelName;
        //  document.getElementById("trHtlNameFliter").style.display = "block";
        //document.getElementById(txtHotelName).value = "";
        SightResultXML();
    }
    else
        alert('Please fill Hotel Name');
}


function ClearHtlNameFilter() {
    debugger;
    // document.getElementById("lblHtlName").innerHTML = "";
    document.getElementById("cph_main_Refine_SName").value = "";
    //  document.getElementById("trHtlNameFliter").style.display = "none";
    SightResultXML();
}


function resultSort(sortType) {
    debugger;
    var sortDType;
    if (sortType == "Min_Price") {
        sortDType = "number";
    }

    else {
        sortDType = "text";
    }
    SightResultFilter(sortType, "");

}