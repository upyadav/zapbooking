﻿function show(id) {
    document.getElementById(1).style.display = 'none';
    document.getElementById(id).style.display = '';
    // fade(id);
}
function hide(id) {
    document.getElementById(id).style.display = 'none';
	 document.getElementById(1).style.display = '';
}

var TimeToFade = 250.0;

function fade(eid) {
    var element = document.getElementById(eid);
    if (element == null)
        return;

    if (element.FadeState == null) {
        if (element.style.opacity == null
        || element.style.opacity == ''
        || element.style.opacity == '0.8') {
            element.FadeState = -2;
        }
        else {
            element.FadeState = 2;
        }
    }

    if (element.FadeState == 1 || element.FadeState == -1) {
        element.FadeState = element.FadeState == 1 ? -1 : 1;
        element.FadeTimeLeft = TimeToFade - element.FadeTimeLeft;
    }
    else {
        element.FadeState = element.FadeState == 2 ? -1 : 1;
        element.FadeTimeLeft = TimeToFade;
        element.style.display = element.FadeState == 2
        ? 'none' : 'block';
        setTimeout("animateFade(" + new Date().getTime()
        + ",'" + eid + "')", 33);
    }
}

function animateFade(lastTick, eid) {
    var curTick = new Date().getTime();
    var elapsedTicks = curTick - lastTick;

    var element = document.getElementById(eid);

    if (element.FadeTimeLeft <= elapsedTicks) {
        element.style.display = element.FadeState == 1
        ? 'block' : 'none';
        element.style.opacity = element.FadeState == 1
        ? '.85' : '0';
        element.style.filter = 'alpha(opacity = '
        + (element.FadeState == 1 ? '85' : '0') + ')';
        element.FadeState = element.FadeState == 1 ? 2 : -2;
        return;
    }

    element.FadeTimeLeft -= elapsedTicks;
    var newOpVal = element.FadeTimeLeft / TimeToFade;
    if (element.FadeState == 1)
        newOpVal = 1 - newOpVal;

    newOpVal = newOpVal * 0.85;
    element.style.opacity = newOpVal;
    element.style.filter =
      'alpha(opacity = ' + (newOpVal * 100) + ')';

    setTimeout("animateFade(" + curTick
      + ",'" + eid + "')", 33);
}