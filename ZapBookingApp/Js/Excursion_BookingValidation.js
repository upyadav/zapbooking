﻿

///////////////////////////////// Form Submit /////////////////////////////////////////////////////////

function Validate_Me() {

    if (document.getElementById("HDestTo").value == '') {
        alert("Please enter destination!");
        document.getElementById("HDestTo").focus();
        return false;
    }
    if (document.getElementById("HDestTo").value == ' ') {
        alert("Please enter valid destination!");
        document.getElementById("HDestTo").focus();
        return false;
    }
    if (document.getElementById("HDestTo").value == 'Please Enter your Destination') {
        alert("Please enter destination!");
        document.getElementById("HDestTo").focus();
        return false;
    }
    if (document.getElementById("txt_ChekinDate").value == '') {
        alert("Please select a Check In Date !");
        document.getElementById("txt_ChekinDate").focus();
        return false;
    }
    if (document.getElementById("txt_ChekOut").value == '') {
        alert("Please select a Check Out Date !");
        document.getElementById("txt_ChekOut").focus();
        return false;
    }
    else {
        return true;
    }
}

function is_valid() {
    var tot_pax = document.frm.TotalPaxs.value;
    if (Number(tot_pax) == 1) {
        if (document.getElementById("txt_pax_Fname1").value == "") {

            alert('Please fill in First Name.');
            document.getElementById("txt_pax_Fname1").focus();
            return false;
        }
        if (document.getElementById("txt_pax_Lname1").value == "") {
            alert('Please fill in Last Name.');
            document.getElementById("txt_pax_Lname1").focus();
            return false;
        }
    }
    if (Number(tot_pax) > 1) {
        for (var i = 1; i <= tot_pax; i++) {
            if (document.getElementById("txt_pax_Fname" + i).value == "") {
                alert('Please enter the Pax First Name!');
                document.getElementById("txt_pax_Fname" + i).focus();
                return false;
            }
            if (document.getElementById("txt_pax_Lname" + i).value == "") {
                alert('Please enter the Pax Last Name!');
                document.getElementById("txt_pax_Lname" + i).focus();
                return false;
            }
        }
    }

    if (document.frm.chkConditions.checked == false) {
        alert("Please read and agree to the enclosed terms and conditions before proceeding to next step.");
        return false;
    }

    if (document.frm.chkConditions.checked == false) {
        document.frm.btnSubmit.disabled = false;
    }
    //added on 13/10/2011 by anand gupta
    var tco = document.getElementById('tcoadditon');
    if (tco != null) {
        var childs = document.getElementById('tcoadditon').getElementsByTagName('input');
        if (childs.length > 0) {
            for (var i = 0; i < childs.length; i++) {
                if (childs[i].value == '') {
                    alert('Please fill all Additinal information !');
                    childs[i].focus();
                    return false;
                }
            }
        } 
    }
    //added on 24/12/2011 by anand gupta
    var hb = document.getElementById('hbcontainer');
    if (hb != null) {
        var childs1 = document.getElementById('hbcontainer').getElementsByTagName('input');
        if (childs1.length > 0) {
            for (var i = 0; i < childs1.length; i++) {
                if (childs1[i].value == '') {
                    alert('Please fill all Additinal information !');
                    childs1[i].focus();
                    return false;
                }
            }
        }
    }
    //added on 17/01/2012 by anand gupta calling validation control
    if (!Page_ClientValidate()) {
        return false;
    }
    return true;
}

function allow_me() {

    document.getElementById("chkConditions").checked = true;
    //document.getElementById("chkConditions").disabled = false;
    document.getElementById("btnSubmit").disabled = false;
    return true;
}

function checkDates() {

    var strChekinDate = document.frmHotelSearch.txt_ChekinDate.value;

    var strChekOutDate = document.frmHotelSearch.txt_ChekOut.value;

    var ch = strChekinDate.substring(0, 11).split(' ');
    var out = strChekOutDate.substring(0, 11).split(' ');
    var one_day = 1000 * 60 * 60 * 24;
    var ChekinDate = new Date(ch[2], ch[1], ch[0]);
    var ChekOutDate = new Date(out[2], out[1], out[0]);
    var dateDiff = Math.ceil((ChekOutDate.getTime() - ChekinDate.getTime()) / (one_day));
    if (dateDiff >= 0) {
        return true;

    } else { return false; }
}

function getKeyCode(e) {
    if (window.event)
        return window.event.keyCode;
    else if (e)
        return e.which;
    else
        return null;
}


function keyRestrict(e, validchars) {
    var key = '', keychar = '';
    key = getKeyCode(e);
    if (key == null) return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();
    validchars = validchars.toLowerCase();
    if (validchars.indexOf(keychar) != -1)
        return true;
    if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
        return true;
    return false;
}
function isEmpty(val) {
    reWhiteSpace = new RegExp(/^\s+$/);
    if (reWhiteSpace.test(val)) {
        return true;
    }
    if (val.match(/^s+$/) || val == "") {
        return true;
    }
    else {
        return false;
    }
}

function isNumber(val) {
    if (isNaN(val)) {
        return false;
    }
    else {
        return true;
    }
}

function isWithinRange(val, min, max) {
    if (val.length >= min && val.length <= max) {
        return true;
    }
    else {
        return false;
    }
}

function isChecked(obj) {
    if (obj.checked) {
        return true;
    }
    else {
        return false;
    }
}

function IsNumeric(strString) //  check for valid numeric strings	
{
    if (!/\D/.test(strString)) return true; //IF NUMBER
    //else if(/^\d+\.\d{2}$/.test(strString)) return true;//IF A DECIMAL NUMBER HAVING AN INTEGER ON EITHER SIDE OF THE DOT(.)
    else if (strString.match(/^\d+\.\d{2}$/)) {
        return true;
    }
    else return false;
}


function hasWhiteSpace(s) {
    reWhiteSpace = new RegExp(/^\s+$/);

    // Check for white space
    if (reWhiteSpace.test(s)) {
        alert("Please Check Your Fields For Spaces");
        return false;
    }
    return true;
}


function validateEmail(strEmail) {
    validRegExp = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;

    if (strEmail.search(validRegExp) == -1) {
        return false;
    }
    return true;
}


function Validate_AddAgent() {

    if (document.getElementById("txtFname").value == '') {
        alert("First Name is required!");
        document.getElementById("txtFname").focus();
        return false;
    }
    if (document.getElementById("txtLastname").value == '') {
        alert("Last Name is requierd!");
        document.getElementById("txtLastname").focus();
        return false;
    }
    if (document.getElementById("txtEmail").value == '') {
        alert("Email  is requierd!");
        document.getElementById("txtEmail").focus();
        return false;
    }
    if (document.getElementById("txtaddrs").value == '') {
        alert("Address  is requierd !");
        document.getElementById("txtaddrs").focus();
        return false;
    }
    if (document.getElementById("txtUname").value == '') {
        alert("UserName is requierd !");
        document.getElementById("txtUname").focus();
        return false;
    }
    ///////////////
    if (document.getElementById("txtPwd").value == '') {
        alert("Password is requierd!");
        document.getElementById("txtPwd").focus();
        return false;
    }
    if (document.getElementById("txtCmpny").value == '') {
        alert("Company Name is requierd!");
        document.getElementById("txtCmpny").focus();
        return false;
    }
    if (document.getElementById("txtCmpnyId").value == '') {
        alert("Company ID!");
        document.getElementById("txtCmpnyId").focus();
        return false;
    }
    if (document.getElementById("drpEmergencyFlag").value == '') {
        alert("Emergency flag is requierd !");
        document.getElementById("drpEmergencyFlag").focus();
        return false;
    }
    if (document.getElementById("txtEmegCName").value == '') {
        alert("Contact Name is requierd !");
        document.getElementById("txtEmegCName").focus();
        return false;
    }
    else {
        return true;
    }
}
