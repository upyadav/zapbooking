import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
        <div class="container">
            
            <router-outlet></router-outlet>
        </div>
    `,
    providers: []
})
export class AppComponent implements OnInit {

    constructor() {
        console.log('AppComponent -> constructor');
    }

    ngOnInit() {
        console.log('AppComponent -> ngOnInit');
    }
}