﻿export interface Weather {
    Master: Master;
    Authentication: Authentication;
    HotelBoard: HotelBoard;
    Filter: Filter;
    Rooms: Rooms;
}
export interface Master {
    CityName: string;
    CityCode: string;
    AreaFlag: string;
    AreaCode: string;
    ChkInDate: string;
    ChkOutDate: string;
    Duration: string;
    CountryName?: null;
    CountryCode: string;
    Currency: string;
    LangCode: string;
    Nationality: string;
    GMax_Price?: null;
    GMin_Price?: null;
    LanCount?: null;
}
export interface Authentication {
    Channel: string;
    CompanyId: string;
    ServiceType: string;
    ClientId: string;
    SessionId: string;
    UserId: string;
    RefNo: string;
}
export interface HotelBoard {
    Type: string;
    Code: string;
}
export interface Filter {
    StarCategory: string;
    HotelName: string;
    AvailableOnly: string;
}
export interface Rooms {
    Room?: (RoomEntity)[] | null;
}
export interface RoomEntity {
    Adult: number;
    RoomType: string;
    Children: Children;
}
export interface Children {
    Child?: (ChildEntity)[] | null;
}
export interface ChildEntity {
    ChildAge: string;
}
